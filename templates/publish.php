<?php

$current_type = strval( get_query_var( 'type' ) ) ?? null;
$form_errors = array();

if ( isset( $_SERVER['REQUEST_METHOD'] ) && 'POST' === $_SERVER['REQUEST_METHOD'] ) {
	if ( \IMCPress\FrontendForm\PostTypeFormValidator::check_nonce( (array) $_REQUEST, (string) $current_type ) ) {
		if ( \IMCPress\FrontendForm\PostTypeFormValidator::check_captcha( (array) $_REQUEST ) ) {
			$validator = new \IMCPress\FrontendForm\PostTypeFormValidator( (array) $_REQUEST, (string) $type );
			$newpost = $validator->save();
			if ( array_key_exists( 'id', $newpost ) ) {
				wp_safe_redirect( get_permalink( $newpost['id'] ) );
				exit;
			} else {
				$form_errors = $newpost;
			}
		} else {
			$form_errors[] = array(
				'name' => 'CaptchaError',
				'message' => __( 'Wrong captcha given', 'imcpress' ),
			);
		}
	} else {
		$form_errors[] = array(
			'name' => 'NonceError',
			'message' => __( 'Invalid nonce', 'imcpress' ),
		);
	}
}

get_header();
do_action('pre_publish_form');

if ( ! empty( $form_errors) ) {
	$html = '';
	foreach ( $form_errors as $error ) {
		$html .= '<li><strong>' . $error['name'] . ':</strong> ' . $error['message'] . '</li>';
	}
	$notice = __( 'There are errors in your publication:', 'imcpress' );
	echo '<div class="notice notice-error">
	<p><strong>' . $notice . '</strong></p>
	<ul> ' . $html . ' </ul>
	</div>';

}

if ( isset( $_SERVER['REQUEST_METHOD'] ) && 'GET' === $_SERVER['REQUEST_METHOD'] ) {
	if ( ! $current_type ) {
		$publish_page = \IMCPress\FrontendForm\PublishPage::get_instance();
		$publish_page->display_descriptions();
	}
}

if ( $current_type ) {
	echo do_shortcode( '[' . $current_type . '_form]' );
}

do_action('post_publish_form');
get_footer();

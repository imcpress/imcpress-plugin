document.addEventListener("DOMContentLoaded", function(){
	function toggleFirstForm( details ) {
		content = document.getElementById("comment-form-details-content");
		// Only if form has not been fetched yet
		if ( content.innerHTML == '' ) {
			// URL to AJAX
			const ajaxurl = details.getAttribute('data-ajaxurl');

			const data = {
				action: details.getAttribute('data-action'),
				nonce: details.getAttribute('data-nonce'),
				postid: details.getAttribute('data-postid')
			}

			console.log(data);

			fetch( ajaxurl, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Cache-Control': 'no-cache',
				},
				body: new URLSearchParams(data)
			})
			.then(response => response.json())
			.then(response => {
				if(!response.success) {
					console.log('Error in response')
					return;
				}
				content.innerHTML = response.data;
			});
		}
	}

	details = document.getElementById('comment-form-details');
	details.addEventListener( 'toggle', (e) => toggleFirstForm( details ) );
});

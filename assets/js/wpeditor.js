<script type='text/javascript'>
var settings = {
	mediaButtons: true,
    tinymce: {
      "theme":"modern",
      "skin":"lightgray",
      "language":"fr",
      "formats":{
         "alignleft":[
            {
               "selector":"p,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
               "styles":{
                  "textAlign":"left"
               }
            },
            {
               "selector":"img,table,dl.wp-caption",
               "classes":"alignleft"
            }
         ],
         "aligncenter":[
            {
               "selector":"p,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
               "styles":{
                  "textAlign":"center"
               }
            },
            {
               "selector":"img,table,dl.wp-caption",
               "classes":"aligncenter"
            }
         ],
         "alignright":[
            {
               "selector":"p,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
               "styles":{
                  "textAlign":"right"
               }
            },
            {
               "selector":"img,table,dl.wp-caption",
               "classes":"alignright"
            }
         ],
         "strikethrough":{
            "inline":"del"
         }
      },
      "relative_urls":false,
      "remove_script_host":false,
      "convert_urls":false,
      "browser_spellcheck":true,
      "fix_list_elements":true,
      "entities":"38,amp,60,lt,62,gt",
      "entity_encoding":"raw",
      "keep_styles":false,
      "resize":"vertical",
      "menubar":false,
      "branding":false,
      "preview_styles":"font-family font-size font-weight font-style text-decoration text-transform",
      "end_container_on_empty_block":true,
      "wpeditimage_html5_captions":true,
      "wp_lang_attr":"fr-FR",
      "wp_keep_scroll_position":false,
      "wp_shortcut_labels":{
         "Heading 2":"access2",
         "Heading 3":"access3",
         "Heading 4":"access4",
         "Heading 5":"access5",
         "Heading 6":"access6",
         "Paragraph":"access7",
         "Blockquote":"accessQ",
         "Underline":"metaU",
         "Strikethrough":"accessD",
         "Bold":"metaB",
         "Italic":"metaI",
         "Code":"accessX",
         "Align center":"accessC",
         "Align right":"accessR",
         "Align left":"accessL",
         "Justify":"accessJ",
         "Cut":"metaX",
         "Copy":"metaC",
         "Paste":"metaV",
         "Select all":"metaA",
         "Undo":"metaZ",
         "Redo":"metaY",
         "Bullet list":"accessU",
         "Numbered list":"accessO",
         "Insert/edit image":"accessM",
         "Remove link":"accessS",
         "Toolbar Toggle":"accessZ",
         "Insert Read More tag":"accessT",
         "Insert Page Break tag":"accessP",
         "Distraction-free writing mode":"accessW",
         "Keyboard Shortcuts":"accessH"
      },
      "toolbar1":"formatselect,bold,italic,underline,blockquote | bullist,numlist | link,unlink | undo,redo | removeformat",
      "wpautop":true,
      "indent":true,
      "elementpath":false,
      "plugins":"charmap,wordpress,wpautoresize,wpeditimage,wpemoji,wpgallery,wplink,wptextpattern,paste",
      "block_formats":'Paragraph=p;Header 1=h2;Header 2=h3;Header 3=h4;Header 4=h5;Header 5=h6;Preformated=pre',
      "paste_use_dialog":true,
      "paste_auto_cleanup_on_paste":true,
      "smart_paste":false,
      "paste_preprocess":function(editor, args){
          var whitelist = 'a,p,span,b,strong,i,em,ul,li,ol,blockquote';
          var stripped = jQuery('<div>' + args.content + '</div>');
          var els = stripped.find('*').not(whitelist);
          for (var i = els.length - 1; i >= 0; i--) {
              var e = els[i];
              jQuery(e).replaceWith(e.innerHTML);
          }
          stripped.find('*').removeAttr('id').removeAttr('class');
          args.content = stripped.html();
       },
   },
   "quicktags":{
      "buttons":"strong,em,link,ul,ol,li,code"
   },
};
wp.editor.initialize( 'post_content', settings);
</script>

<?php

namespace IMCPress;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class AbstractManager {

    public array $slugs = array();
    protected static string $managed_class;

    public function __construct() {}

	public function get_slugs(): array {
		return $this->slugs;
	}

	public function setup( array $options ): void {
		$managed = new static::$managed_class( $options );
		add_action( 'init', array( $managed, 'register' ) );
		array_push( $this->slugs, $options['slug'] );
	}

}

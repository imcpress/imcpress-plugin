<?php

namespace IMCPress\Metabox;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Display {

	public string $metadatum_id;
	public $metadatum_value;
	public string $metadatum_type;
	public string $metadatum_label;
	public string $metadatum_desc;
	public string $metadatum_slug;
	public string $metadatum_default;
	public array $metadatum_options;
	public array $element_options;
	public string $html;

	public function __construct( string $metadatum_id, $metadatum_value = '', array $metadatum, array $element_options = array() ) {
		$this->metadatum_id      = $metadatum_id;
		$this->metadatum_value   = $metadatum_value;
		$this->metadatum_type    = $metadatum['type'] ?? 'text';
		$this->metadatum_label   = $metadatum['label'] ?? '';
		$this->metadatum_desc    = $metadatum['desc'] ?? '';
		$this->metadatum_slug    = $metadatum['slug'] ?? '';
		$this->metadatum_default = $metadatum['default'] ?? '';
		$this->metadatum_options = $metadatum['options'] ?? array();
		$default_options = array(
			'required'	=> true,
		);
		$this->element_options   = wp_parse_args( $element_options, $default_options );
		$this->html              = call_user_func( array( $this, $this->metadatum_type ) );
	}

	public function hidden(): string {
		return '<input type="hidden" name="' . $this->metadatum_id . '" id="' . $this->metadatum_id . '" value="' . $this->metadatum_value . '" class="widefat" />';
	}

	public function select(): string {
		$label_for   = isset( $this->element_options['label_for'] ) ? $this->element_options['label_for'] : $this->metadatum_id;
		$select_name = isset( $this->element_options['select_name'] ) ? $this->element_options['select_name'] : $this->metadatum_id;
		$select_id   = isset( $this->element_options['select_id'] ) ? $this->element_options['select_id'] : $this->metadatum_id;
		$attributes  = isset( $this->element_options['select_attributes'] ) ? $this->element_options['select_attributes'] : '';

		$html  = '';
		$html .= '<p class="attribute-wrapper"><label class="post-attributes-label" for="' . $label_for . '">' . $this->metadatum_label . '</label></p>';
		$html .= '<p><select name="' . $select_name . '" id="' . $select_id . '" ' . $attributes . ' >';
		foreach ( $this->metadatum_options as $this->metadatum_option_value => $this->metadatum_option_label ) {
			$html .= '<option ' . ( $this->metadatum_value ? selected( $this->metadatum_option_value, $this->metadatum_value, false ) : '' ) . ' value="' . $this->metadatum_option_value . '">' . $this->metadatum_option_label . '</option>';
		}

		$html .= '</select>';
		$html .= '</p>';
		$html .= '<div class="metadata-desc"><label for="'.$label_for.'">' . $this->metadatum_desc . '</label></div>';

		return $html;

	}

	public function textarea(): string {
		$name = isset( $this->element_options['input_name'] ) ? $this->element_options['input_name'] : $this->metadatum_id;
		if ( ! is_admin() && empty( $this->metadatum_value ) ) {
				$this->metadatum_value = wp_kses_post( self::get_value_from_request( $name ) );
		}

		$required = $this->element_options['required'] ? ' required':'';

		$attributes = isset( $this->element_options['input_attributes'] ) ? $this->element_options['input_attributes'] : '';
		$html  = '';
		$html .= '<p class="post-attributes-label-wrapper"><label class="post-attributes-label" for="' . $this->metadatum_id . '">' . $this->metadatum_label . '</label></p>';
		$html .= '<textarea name="' . $name . '" id="' . $name . '" class="widefat" ' . $attributes . $required .'>' . $this->metadatum_value . '</textarea>';
		$html .= '<div class="metadata-desc"><label for="'.$name.'">' . $this->metadatum_desc . '</label></div>';
		return $html;
	}

	public function checkbox(): string {
		$name = isset( $this->element_options['input_name'] ) ? $this->element_options['input_name'] : $this->metadatum_id;
		if ( ! is_admin() && empty( $this->metadatum_value ) ) {
				$this->metadatum_value = wp_kses_post( self::get_value_from_request( $name ) );
		}
		$attributes = isset( $this->element_options['input_attributes'] ) ? $this->element_options['input_attributes'] : '';
		$html  = '';
		$html .= '<p class="post-attributes-label-wrapper"><label class="post-attributes-label" for="' . $this->metadatum_id . '">' . $this->metadatum_label . '</label></p>';
		$html .= '<input type="hidden" name="' . $name . '" " class="widefat" ' . $attributes . ' value="0" ' . $this->metadatum_value . '/>';
		$html .= '<input type="checkbox" name="' . $name . '" id="' . $name . '" class="widefat" ' . $attributes . ' value="1" ' . checked( $this->metadatum_value, 1, false) . '/>';
		$html .= '<label for="'.$name.'" class="metadata-desc">' . $this->metadatum_desc . '</div>';
		return $html;
	}

	public function multiple_checkboxes_categorized(): string {
		$name = isset( $this->element_options['input_name'] ) ? $this->element_options['input_name'] : $this->metadatum_id;
		if ( ! is_admin() && empty( $this->metadatum_value ) ) {
				$this->metadatum_value = wp_kses_post( self::get_value_from_request( $name ) );
		}
		$attributes = isset( $this->element_options['input_attributes'] ) ? $this->element_options['input_attributes'] : '';
		
		$html  = '';
		$html .= '<p class="post-attributes-label-wrapper"><label class="post-attributes-label" for="' . $this->metadatum_id . '">' . $this->metadatum_label . '</label></p>';
		
		foreach ( $this->metadatum_options as $category => $boxes ) {
			$html .= '<p class="post-attributes-label-wrapper"><b>' . $category . '</b></p>';
			
			foreach ( $boxes as $name)
			{
				$id = sprintf( '%s[%s][%s]', $this->metadatum_id, $category, $name);
				$html .= '<input type="checkbox" name="' . $id . '" id="' . $id . '" class="widefat" ' . $attributes . ' value="'.$name.'" ' . checked( isset( $this->metadatum_value[$category][$name] ), true, false) . '/>';
				$html .= '<label for="'.$id.'" class="metadata-desc">' . $name . '</label></div> ';
			}
			$html .= '<br><br>';
		}

		$html .= '<div class="metadata-desc">' . $this->metadatum_desc . '</div>';
		return $html;
	}

	public function datetime(): string {
		$datetime_format = get_option( 'date_format' ) . ' H:i';
		if ( ! empty( $this->metadatum_value ) ) {
			$datetime = new \DateTime( $this->metadatum_value );
		} else {
			$post_value = self::get_value_from_request( $this->metadatum_id );
			if ( ! empty( $post_value ) ) {
				$datetime = \DateTime::createFromFormat( $datetime_format, $post_value );
			} else {
				$datetime = new \DateTime( 'now', wp_timezone() );
			}
		}

		$required = $this->element_options['required'] ? ' required':'';

		$html  = '';
		$html .= '<p class="post-attributes-label-wrapper"><label class="post-attributes-label" for="' . $this->metadatum_id . '">' . $this->metadatum_label . '</label></p>';

		$html .= '<div class="metadata-desc"><label for="'.$this->metadatum_id.'">' . $this->metadatum_desc . '</label></div>';
		$html .= '<input id="datetimepicker" name="' . $this->metadatum_id . '" type="text" value="' . $datetime->format( $datetime_format ) . '"'.$required.'/>';
		$html .= '	  <label class="metadata-desc dashicons dashicons-calendar" for="datetimepicker"></label>';
		$html .= '<script type="text/javascript">
			jQuery.datetimepicker.setLocale("' . explode( '_', get_locale() )[0] . '");
			jQuery("#datetimepicker").datetimepicker({
			format:"' . $datetime_format . '",
			dayOfWeekStart:"' . get_option( 'start_of_week' ) . '",
			step:15});
</script>';
		return $html;

	}

	public function text(): string {
		$name = isset( $this->element_options['input_name'] ) ? $this->element_options['input_name'] : $this->metadatum_id;
		if ( ! is_admin() && empty( $this->metadatum_value ) ) {
			if ( ! preg_match( '/^tax_input.+/' , $name ) && 'captcha' !== $name ) {
				$this->metadatum_value = wp_strip_all_tags( self::get_value_from_request( $name ) );
			}
		}

		$attributes = isset( $this->element_options['input_attributes'] ) ? $this->element_options['input_attributes'] : '';
		$required = $this->element_options['required'] ? ' required':'';

		$html  = '';
		$html .= '<p class="post-attributes-label-wrapper"><label class="post-attributes-label" for="' . $this->metadatum_id . '">' . $this->metadatum_label . '</label></p>';
		$html .= '<p><input type="' . $this->metadatum_type . '" name="' . $name . '" id="' . $this->metadatum_id . '" value="' . $this->metadatum_value . '" ' . $attributes . ' class="widefat"'.$required .'/></p>';
		if ( ! empty( $this->metadatum_desc ) ) {
			$html .= '<label for="'.$name.'">';
			$html .= $this->metadatum_desc;
			$html .= '</label>';
		}
		return $html;
	}

	public static function taxonomy_dropdown( \WP_Post $post, array $box ): void {

		$html = '';
		$taxonomy = get_taxonomy( esc_attr( $box['args']['taxonomy'] ) );
		$box_id   = (string) esc_attr( $box['id'] );

		$selected = '';
		if ( 0 !== $post->ID ) {
			$assigned = get_the_terms( $post->ID, $taxonomy->name );
			if ( ( $assigned ) && ( is_array( $assigned ) ) ) {
				$selected = (string) $assigned[0]->slug;
			}
		} elseif ( ! is_admin() ) {
			$selected = wp_strip_all_tags(
				self::get_value_from_request(
					'',
					array(
						'tax_input',
						$taxonomy->name,
					),
				)
			);
		}

		$post_terms = get_terms(
			array(
				'taxonomy'   => $taxonomy->name,
				'hide_empty' => false,
			)
		);
		$select_options = array();
		foreach ( $post_terms as $term ) {
			$select_options[ (string) $term->slug ] = $term->name;
		}

		$metabox = new \IMCPress\Metabox\Display(
			$taxonomy->name,
			$selected,
			array(
				'type'    => 'select',
				'label'   => $taxonomy->labels->add_new_item,
				'options' => $select_options,
			),
			array(
				'label_for'         => 'tag-' . $taxonomy->name,
				'select_name'       => 'tax_input[' . $taxonomy->name . ']',
				'select_id'         => 'tag-' . $taxonomy->name,
				'select_attributes' => 'data-wp-taxonomy="' . $taxonomy->name . '" aria-describedby="tag-' . $taxonomy->name . '-desc" role="combobox" aria-owns="ui-id-3" ',
			),
		);

		$html .= '<div class="tagsdiv" id="' . $box_id . '">';
		$html .= $metabox->html;
		$html .= '<span class="howto" id="tag-' . $taxonomy->name . '-desc">' . $taxonomy->labels->separate_items_with_commas . '</span>';
		$html .= '</div>';

		echo $html;
	}

	public static function taxonomy_autocomplete( \WP_Taxonomy $taxonomy ): string {

		$taxonomies = wp_strip_all_tags(
			self::get_value_from_request(
				'',
				array(
					'tax_input',
					$taxonomy->name,
				),
			)
		);

		$metabox = new \IMCPress\Metabox\Display(
			$taxonomy->name,
			$taxonomies,
			array(
				'type'  => 'text',
				'label' => $taxonomy->labels->add_new_item,
			),
			array(
				'input_name' => 'tax_input[' . $taxonomy->name . ']',
				'input_attributes' => ' data-wp-taxonomy="' . $taxonomy->name . '"',
			),
		);
		$html = '';
		$html .= '<div class="tagsdiv" id="' . $taxonomy->name . '">';
		$html .= $metabox->html;
		$html .= '<span class="howto" id="tag-' . $taxonomy->name . '-desc">' . $taxonomy->labels->separate_items_with_commas . '</span>';
		$html .= '<span class="howto">'.__( 'Go on <a href="/about/">tags page</a> to see a list of existings tags', 'imcpress' ).'</span>';
		$html .= '</div>';
		return $html;
	}

	private static function get_value_from_request( string $element, array $elements = array() ): string {
		if ( isset( $_SERVER['REQUEST_METHOD'] ) && 'POST' === $_SERVER['REQUEST_METHOD'] ) {
			if ( ! empty( $element ) ) {
				if ( isset( $_REQUEST ) && array_key_exists( $element, $_REQUEST ) ) {
					return (string) wp_unslash( $_REQUEST[ $element ] );
				}
			} elseif ( ! empty( $elements ) ) {
				if ( isset( $_REQUEST ) && array_key_exists( $elements[1], $_REQUEST[ $elements[0] ] ) ) {
					return (string) wp_unslash( $_REQUEST[ $elements[0] ][ $elements[1] ] );
				}
			}
		}
		return '';
	}

}

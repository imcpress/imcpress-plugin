<?php

namespace IMCPress\Metabox;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Sanitize {

	public string $type;

	public function __construct( string $type ) {
		$this->type = esc_attr( $type );
	}

	public function sanitize( $meta_value ) {
		return call_user_func(
			array(
				$this,
				$this->type,
			),
			$meta_value,
		);
	}

	public function hidden( string $meta_value ): string {
		return sanitize_text_field( $meta_value );
	}

	public function select( string $meta_value ): string {
		return sanitize_text_field( $meta_value );
	}

	public function text( string $meta_value ): string {
		return sanitize_text_field( $meta_value );
	}

	public function checkbox( string $meta_value ): string {
		return $meta_value ? 1 : 0;
	}

	public function multiple_checkboxes_categorized( array $meta_value ): array {
		// Fix: what should we sanitize here?
		return $meta_value;
	}

	public function textarea( string $meta_value ): string {
		return sanitize_textarea_field( $meta_value );
	}

	public function datetime( string $meta_value ): string {
		$wp_datetime_format = get_option( 'date_format' ) . ' H:i';
		$datetime_field     = \DateTime::CreateFromFormat(
			$wp_datetime_format,
			$meta_value,
			new \DateTimeZone( 'UTC' ),
		);
		return $datetime_field->format( 'c' );
	}
}

<?php

namespace IMCPress;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Rewrite {

	public static function plugins_loaded(): void {
		// Rewrite rules
		add_filter( 'registered_taxonomy', array( __CLASS__, 'registered_taxonomy' ), 10, 3 );
		add_filter( 'registered_post_type', array( __CLASS__, 'registered_custom_post_type' ), 10, 2 );
		add_filter( 'query_vars', array( __CLASS__, 'add_query_vars' ) );
		add_filter( 'generate_rewrite_rules', array( __CLASS__, 'generate_rewrite_rules' ) );
		add_action( 'init', array( __CLASS__, 'init' ), 12 );

		// Modify post types links generation
		add_filter( 'pre_post_link', array( __CLASS__, 'filter_pre_post_link' ) );
		add_filter( 'post_type_link', array( __CLASS__, 'filter_custom_post_link' ), 10, 2 );

		// Prevent 404 when we set manually is_archive to true in pre_get_posts
		add_filter( 'pre_handle_404', array( __CLASS__, 'handle_404' ) );
		add_filter( 'template_include', array( __CLASS__, 'template_include' ) );
	}

	public static function registered_taxonomy( string $name, $types, array $args ): void {
		if ( 'category' === $name ) {
			// Remove category rewrite rules, as we removed category tax
			remove_permastruct( $name );
		} elseif ( 'post_tag' === $name ) {
			// Set tag url to '/about/%term%/'
			remove_permastruct( $name );
			add_permastruct( $name, 'about/%' . $name . '%', $args['rewrite'] );
		} elseif ( 'wire' === $name ) {
			// Edit %wire% tag regex to exclude number containing strings
			// So when we get '/articles/38/' it doesn't think 38 is a wire term (like local/global)
			// (as rewrite rule for taxonomies is tried before post's one)
			// implies wire terms can't contain numbers

			$type = get_taxonomy( $name );
			add_rewrite_tag(
				"%$name%",
				'([^[/0-9]+)',
				$type->query_var ? "{$type->query_var}=" : "taxonomy=$type->name&term="
			);
			// Or make it even more speicific like (local|global)
		}

		// Nothing to do with custom tax? as their properites are set in config/imcpress.php
		// (where we couldn't set builtin properties, which is the reason we're hooking here)
	}

	public static function registered_custom_post_type( string $name, \WP_Post_Type $type ): void {
		// Custom post types
		if ( ! $type->_builtin ) {
			// Reset permastruct as we want them (ie. including post_id)
			remove_permastruct( $name );
			add_permastruct(
				$name,
				$type->rewrite['slug'] .
				'/%post_id%/%' .
				$name .
				'%',
				$type->rewrite
			);
		}
	}

	public static function init(): void {
		global $wp_rewrite;
		$wp_rewrite->author_base = 'groups';

		// Overwrite post permastruct (used to create permalinks)
		$wp_rewrite->permalink_structure = 'posts/%post_id%/%postname%/';
	}

	public static function add_query_vars( array $vars ): array {
		$add = array(
			'list',
			'event_view',
			'post_status',
			'tax',
			'term',
		);

		return array_merge( $vars, $add );
	}

	public static function generate_rewrite_rules( \WP_Rewrite $wpr ) {
		// Set rewrite properties for post
		// and create its posts permastruct/rewrite rules
		$post = get_post_type_object( 'post' );
		// Archive link is determined by rewrite[slug]
		$post->has_archive = true;
		$post->rewrite     = array(
			'slug'       => 'posts',
			'feeds'      => true,
			'pages'      => false,
			'with_front' => false,
		);
		$post->add_rewrite_rules();

		add_rewrite_tag( '%list%', '(about|in|groups)' );
		$structs['list'] = array( 'struct' => '%list%' );

		add_rewrite_tag(
			'%event_view%',
			'(calendar|list|archive)',
			'post_type=imcpress_event&event_view='
		);
		$structs['event_view'] = array(
			'struct' => 'events/%event_view%/%year%/%monthnum%',
		);

		// 'published', as /publish is yet taken for publish page
		add_rewrite_tag(
			'%post_status%',
			'(published|refused|feature|debate|pending)'
		);
		$structs['status'] = array( 'struct' => '%post_status%' );

		// /ctp/tax/term, eg. /events/in/somewhere
		add_rewrite_tag( '%tax%', '(about|in)', 'taxonomy=' );
		add_rewrite_tag( '%term%', '([^/]+)', 'term=' );

		$types = get_post_types(
			array(
				'_builtin' => false,
				'name'     => 'post',
			),
			'object',
			'or'
		);

		foreach ( $types as $type ) {
			$name = $type->name;
			$slug = $type->rewrite['slug'];
			$tag  = '%type_' . $slug . '%';

			$structs[ $slug . '_tax' ] = array(
				'struct'    => $tag . '/%tax%/%term%',
				'walk_dirs' => false,
			);
			add_rewrite_tag(
				$tag,
				$slug . '()',
				'post_type=' . $name . '&dummy='
			);
			// Query vars added here are not used in wp navigation,
			// thoses tags are just tricks to trigger appropriate rules creation
		}

		$defaults = array(
			'with_front'  => false,
			'ep_mask'     => EP_NONE,
			'paged'       => true,
			'feed'        => true,
			'forcomments' => false,
			'walk_dirs'   => true,
			'endpoints'   => true,
			'struct'      => '',
		);

		$extra_rules = array();
		foreach ( $structs as $struct ) {

			$args           = array_intersect_key( $struct, $defaults );
			$args           = wp_parse_args( $args, $defaults );
			$args['struct'] = $wpr->root . $args['struct'];

			$rules = $wpr->generate_rewrite_rules(
				$args['struct'],
				$args['ep_mask'],
				$args['paged'],
				$args['feed'],
				$args['forcomments'],
				$args['walk_dirs'],
				$args['endpoints']
			);

			$extra_rules = array_merge( $extra_rules, $rules );
		}

		$wpr->rules = array_merge( $extra_rules, $wpr->rules );
	}

	// This hook only work for post
	// Overwrite permalinks settings to set post link as /articles/%id%/
	public static function filter_pre_post_link(): string {
		// Hardcode post permastruct to always be like CTP permastructs
		// Overriding permalink settings in admin panel
		global $wp_rewrite;
		return $wp_rewrite->permalink_structure;
	}

	// Overwrite permalinks settings to set CTP link as /ctp/%id%/%postname%/
	// No need for type check as this hook is only triggered for custom types
	public static function filter_custom_post_link( string $link, \WP_Post $post ): string {
		return str_replace( '/%post_id%/', '/' . $post->ID . '/', $link );
	}

	public static function handle_404(): bool {
			global $wp_the_query;
			return $wp_the_query->is_archive();
	}

	public static function template_include( string $tpl ): string {
		if ( $qv = get_query_var( 'list' ) ) {
			$templates = array(
				"list-{$qv}.php",
				'list.php',
			);
			return locate_template( $templates, false, true, array( 'list' => $qv ) );
		} elseif ( $qv = get_query_var( 'event_view' ) ) {
			if ( 'archive' !== $qv ) {
				return locate_template( "events_$qv.php" );
			}
		}

		return $tpl;
	}

	// Return rewrite['slug'] property of a taxonomy
	public static function get_tax_rewrite_slug( string $tax ): ?string {
		if ( $tax = get_taxonomy( $tax ) ) {
			return $tax->rewrite['slug'] ?? null;
		}
		return null;
	}

}

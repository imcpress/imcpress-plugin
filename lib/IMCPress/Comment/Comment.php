<?php

namespace IMCPress\Comment;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Comment {

	public static array $disapproved = array();

	public static function set_disapproved_settings(): void {
		self::$disapproved = get_option( 'disapproved_comments' );
	}

	public static function plugins_loaded(): void {

		// Comment form
		add_filter( 'comment_form_default_fields', array( __CLASS__, 'default_comment_fields' ) );
		add_filter( 'comment_form_fields', array( __CLASS__, 'comment_form_fields' ) );
		add_filter( 'pre_comment_author_url', array( __CLASS__, 'remove_author_url' ) );
		add_filter( 'comment_form_field_author', array( __CLASS__, 'logged_author_field' ) );
		add_action( 'comment_form_logged_in_after', array( __CLASS__, 'logged_in_text' ) );

		// Ajax form
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ) );
		add_action( 'wp_ajax_imcpress_comment_form', array( __CLASS__, 'ajax_comment_form') );
		add_action( 'wp_ajax_nopriv_imcpress_comment_form', array( __CLASS__, 'ajax_comment_form') );

		// Set status
		add_filter( 'comments_open', array( __CLASS__, 'comments_opened' ), 10, 2 );
		add_action( 'comment_post', array( __CLASS__, 'update_comment_status' ), 10, 3 );
		add_action( 'save_post', array( __CLASS__, 'set_default_post_comment_status' ), 15, 3 );

		// Admin comments columns
		add_filter( 'manage_edit-comments_columns', array( __CLASS__, 'set_comments_columns' ) );
		add_action( 'manage_comments_custom_column', array( __CLASS__, 'title_column_content' ), 10, 2 );

		add_action( 'trashed_comment', array( __CLASS__, 'post_moderation_comment' ), 10, 2 );
	}

	public static function enqueue_scripts(): void {
		if ( is_single() && apply_filters( 'imcpress_enqueue_ajax_form', false ) ) {
			wp_enqueue_script(
				'imcpress-ajax',
				IMCPRESS_URL . 'assets/js/imcpress-ajax.js',
			);
		}
	}

	public static function ajax_comment_form() {
		// Check nonce
		if( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'imcpress_comment_form' ) ) {
			wp_send_json_error( __( 'Sorry, you are not allowed to do that.' ), 403 );
		}

		// If postid is not set, we can't get comment form
		if ( ! isset( $_POST['postid'] ) ) {
			return;
		}
		$postid = (int) $_POST['postid'];

		ob_start();
		comment_form( array(), $postid );
		$html = ob_get_clean();

		// Send data to user
		wp_send_json_success( $html );
	}

	public static function default_comment_fields( $fields ) {
		unset( $fields['url'] );
		unset( $fields['cookies'] );
		unset( $fields['email'] );
		$fields['title']   = '<label for="title">' . __( 'Titre' ) . '</label><input id="title" type="text" name="title" size="30" maxlength="245">';
		if ( ! is_user_logged_in() ) {
			$prefix            = \IMCPress\FrontendForm\PublishPage::generate_captcha();
			$fields['captcha'] = \IMCPress\FrontendForm\PostTypeFormElement::captcha( $prefix );
		}
		return $fields;
	}

	public static function comment_form_fields( array $fields ): array {
		// Set comment field last
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		if ( ! is_user_logged_in() ) {
			$captcha_field = $fields['captcha'];
			unset( $fields['captcha'] );
			$fields['captcha'] = $captcha_field;
		}

		// Override logged in form, cause wp only would display content field
		if ( is_user_logged_in() ) {
			$comment_field_keys = array_diff( array_keys( $fields ), array( 'comment' ) );

			$first_field = reset( $comment_field_keys );
			$last_field  = end( $comment_field_keys );

			foreach ( $fields as $name => $field ) {
				if ( 'comment' === $name ) {
					echo apply_filters( 'comment_form_field_comment', $field );
				} else {
					if ( $first_field === $name ) {
						do_action( 'comment_form_before_fields' );
					}

					echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";

					if ( $last_field === $name ) {
						do_action( 'comment_form_after_fields' );
					}
				}
			}

			return array();
		}

		return $fields;
	}

	public static function set_default_post_comment_status( int $id, \WP_Post $post, bool $update ): void {
		if ( 'auto-draft' === $post->post_status ) {
			return;
		}

		$status = self::get_custom_post_comment_status( $post, true );

		remove_action( 'save_post', array( __CLASS__, 'set_default_post_comment_status' ), 15 );
		wp_update_post(
			array(
				'ID'             => $id,
				'comment_status' => $status ? 'open' : 'closed',
			)
		);
		add_action( 'save_post', array( __CLASS__, 'set_default_post_comment_status' ), 15, 3 );
	}

	public static function comments_opened( bool $open, int $post_id ): bool {
		// not if refused + !post_type_supports($type, 'comments')
		$post = get_post( $post_id );

		if ( null === $post ) {
			return false;
		}

		if ( 'refused' === $post->post_status ) {
			return false;
		}

		if ( ! post_type_supports( $post->post_type, 'comments' ) ) {
			return false;
		}

		return true;
	}

	public static function update_comment_status( int $comment_ID, $comment_approved, array $data ): void {

		$post = get_post( $data['comment_post_ID'] );
		if ( null === $post ) {
			return;
		}

		$anon_author = __( 'Anonymous' );

		// Make sure no comment cookie is set
		if ( isset( $_POST['wp-comment-cookies-consent'] ) ) {
			unset( $_POST['wp-comment-cookies-consent'] );
		}

		// Check captcha, if wrong, remove comment
		if ( ! is_user_logged_in() ) {
			if ( ! \IMCPress\FrontendForm\PostTypeFormValidator::check_captcha( (array) $_REQUEST ) ) {
				wp_delete_comment( $comment_ID, true );
				wp_die(
					'<p>' . __( 'Wrong captcha entered.', 'imcpress' ) . '</p>',
					__( 'Comment Submission Failure' ),
					array( 'back_link' => true )
				);
			}
		}

		// Get default approval status
		$status = self::get_custom_post_comment_status( $post );
		// Always approved for modadmins
		if ( 0 !== $data['user_ID'] ) {
			$user = get_user_by( 'ID', $data['user_ID'] );
			if ( user_can( $user, 'moderate_comments' ) ) {
				$status = 1;
			}

			$data['comment_author']       = isset( $_POST['author'] ) && ! empty( $_POST['author'] ) ? sanitize_text_field( $_POST['author'] ) : $anon_author;
		}

		// Save title meta
		if ( isset( $_POST['title'] ) ) {
			$title = (string) $_POST['title'];
			$title = sanitize_text_field( $title );
			add_comment_meta( $comment_ID, 'imcpress_comment_title', $title );
		}

		wp_update_comment(
			array(
				'comment_ID'           => $comment_ID,
				'comment_approved'     => $status,
				'comment_author'       => $data['comment_author'],
				'comment_author_email' => '',
				// If logged in user emptied author field, treat it as anon and set user_id to 0
				'user_id'              => $data['comment_author'] === $anon_author ? 0 : $data['user_id'],
			)
		);
	}

	public static function get_custom_post_comment_status( \WP_Post $post, bool $publishing = false ): int {

		if ( 'closed' === $post->comment_status ) {
			return 0;
		}

		if ( 'post' === $post->post_type ) {
			$wire = get_the_terms( $post, 'wire' );
			if ( ! empty( $wire ) ) {
				$wire = $wire[0]->slug;
			} else // If for whatever reason, a wire isn't set?
			{
				return 0;
			}

			if ( array_key_exists( 'wire', self::$disapproved ) && in_array( $wire, self::$disapproved['wire'], true ) ) {
				return 0;
			}
		} else {
			if ( array_key_exists( 'post_type', self::$disapproved ) && in_array( $post->post_type, self::$disapproved['post_type'], true ) ) {
				return 0;
			}
		}

		if ( ! $publishing && array_key_exists( 'status', self::$disapproved ) && in_array( $post->post_status, self::$disapproved['status'], true ) ) {
			return 0;
		}

		return 1;
	}

	public static function remove_author_url(): string {
		return '';
	}

	public static function logged_author_field( $field ) {
		if ( is_user_logged_in() ) {
			$user  = wp_get_current_user();
			$field = str_replace( 'value=""', 'value="' . $user->display_name . '"', $field );
		}
		return $field;
	}

	public static function logged_in_text(): void {
		echo '"' . __( 'Anonymous' ) . '" will be set if name is empty, whatever else will still get your user ID associated wih comment';
	}

	public static function set_comments_columns( $columns ) {
		$add['author']        = $columns['author'];
		$add['comment']       = $columns['comment'];
		$add['imc_com_title'] = 'Title';

		unset( $columns['author'] );
		unset( $columns['comment'] );

		return array_merge( $add, $columns );
	}

	public static function title_column_content( string $column, int $id ): void {
		if ( $column === 'imc_com_title' ) {
			echo get_comment_meta( $id, 'imcpress_comment_title', true );
		}
	}

	public static function post_moderation_comment( int $comment_id, \WP_Comment $comment ): void {
		// set comment dates so that it gets displayed at the right place in the comments thread
		$date     = date_add( new \DateTime( $comment->comment_date ), new \DateInterval( 'PT1S' ) );
		$date_gmt = date_add( new \DateTime( $comment->comment_date_gmt ), new \DateInterval( 'PT1S' ) );
		$moderation_comment = array(
			'comment_author'   => get_option( 'blogname' ),
			'comment_post_ID'  => $comment->comment_post_ID,
			'comment_date'     => $date->format( 'Y-m-d H:i:s' ),
			'comment_date_gmt' => $date_gmt->format( 'Y-m-d H:i:s' ),
			'comment_content'  => __( 'This comment did not respect our editorial policy.', 'imcpress' ),
			'comment_approved' => 1,
			'user_id'          => wp_get_current_user()->ID,
			'comment_meta'     => array(
				'imcpress_comment_title' => __( 'A comment has been hidden', 'imcpress' ),
			),
		);
		wp_insert_comment( $moderation_comment );
	}
}

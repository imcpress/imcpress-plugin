<?php

namespace IMCPress\Comment;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Walker extends \Walker_Comment {

	// Simply copied and adjusted from parent
	protected function html5_comment( $comment, $depth, $args ) {
		$tag                = ( 'div' === $args['style'] ) ? 'div' : 'li';
		$commenter          = wp_get_current_commenter();
		$show_pending_links = ! empty( $commenter['comment_author'] );

		$moderation_note = __( 'Your comment is awaiting moderation. This is a preview; your comment will be visible after it has been approved.' );
		?>
		<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( $this->has_children ? 'parent' : '', $comment ); ?>>
			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<?php
						$comment_author = get_comment_author( $comment );

						if ( '0' == $comment->comment_approved && ! $show_pending_links ) {
							$comment_author = get_comment_author( $comment );
						}

						$title = get_comment_meta( $comment->comment_ID, 'imcpress_comment_title', true );
						$title = empty( $title ) ? '' : ' <b>' . $title . '</b>';
						printf(
							// Fix: i18n namespace?
							/* translators: %s: Comment author link. */
							__( '%1$s <span class="says">says:</span>%2$s' ),
							sprintf( '<b class="fn">%s</b>', $comment_author ),
							$title
						);

						?>
					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<?php
						printf(
							'<a href="%s"><time datetime="%s">%s</time></a>',
							esc_url( get_comment_link( $comment, $args ) ),
							get_comment_time( 'c' ),
							sprintf(
								/* translators: 1: Comment date, 2: Comment time. */
								__( '%1$s at %2$s' ),
								get_comment_date( '', $comment ),
								get_comment_time()
							)
						);

						?>
					</div><!-- .comment-metadata -->

					<?php if ( '0' == $comment->comment_approved ) : ?>
					<em class="comment-awaiting-moderation"><?php echo $moderation_note; ?></em>
					<?php endif; ?>
				</footer><!-- .comment-meta -->

				<div class="comment-content">
					<?php comment_text(); ?>
				</div><!-- .comment-content -->

				<?php
				if ( '1' == $comment->comment_approved || $show_pending_links ) {
					comment_reply_link(
						array_merge(
							$args,
							array(
								'add_below' => 'div-comment',
								'depth'     => $depth,
								'max_depth' => $args['max_depth'],
								'before'    => '<div class="reply">',
								'after'     => '</div>',
							)
						)
					);
				}
				?>
			</article><!-- .comment-body -->
		<?php
	}
}

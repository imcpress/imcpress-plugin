<?php

namespace IMCPress;

use DOMDocument;
use DOMXPath;
use WP_Error;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class IMCPress {

	public static function plugins_loaded(): void {

		Option\Manager::plugins_loaded();

		// Hooks
		add_filter( 'authenticate', array( '\IMCPress\User\Hyperactive', 'authenticate' ), 21, 3 );
		add_filter( 'auto_update_plugin', '__return_false' );
		add_filter( 'auto_update_theme', '__return_false' );
		if ( ! empty( get_option( 'contributor_login' ) ) ) {
			add_filter( 'nonce_user_logged_out', array( '\IMCPress\IMCPress', 'nonce_user_logged_out' ) );
		}
		add_filter( 'media_send_to_editor', array( '\IMCPress\IMCPress', 'media_send_to_editor' ) );
		add_filter( 'translations_api', function() { return true; } );
		add_filter( 'wp_editor_settings', array( '\IMCPress\IMCPress', 'remove_header_one' ), 10, 1 );
		add_filter( 'upload_mimes', array( '\IMCPress\MimeType', 'restrict' ) );
		add_filter( 'robots_txt', array( '\IMCPress\IMCPress', 'filter_robots' ) );
		add_action( 'transition_post_status', array( __CLASS__, 'add_feature_meta' ), 10, 3);
		add_filter( 'the_author', array( 'IMCPress\IMCPress', 'imcp_author' ), 18 );
		add_filter( 'get_the_tags', array( __CLASS__, 'rss_tags'));

		add_action( 'setup_statuses', array( '\IMCPress\IMCPress', 'setup_statuses' ), 1, 1 );
		add_action( 'setup_taxonomies', array( '\IMCPress\IMCPress', 'setup_taxonomies' ), 2, 1 );
		add_action( 'setup_types', array( '\IMCPress\IMCPress', 'setup_types' ), 3, 1 );
		add_action( 'setup_contributor', array( '\IMCPress\IMCPress', 'setup_contributor' ), 4, 1 );
		add_action( 'setup_publish_page', array( '\IMCPress\IMCPress', 'setup_publish_page' ), 5, 1 );
		add_action( 'init', array( '\IMCPress\IMCPress', 'init' ), 2, 0 );

		add_action( 'save_post', array( 'IMCPress\IMCPress', 'filter_hotlinks' ), 10, 2 );
		add_action( 'save_post', array( 'IMCPress\IMCPress', 'set_pending_post_name' ), 10, 2 );
		add_action( 'transition_post_status', array( 'IMCPress\IMCPress', 'add_last_post_user_meta' ), 10, 3 );
		add_action( 'transition_post_status', array( 'IMCPress\IMCPress', 'remove_tags_on_refused' ), 10, 3 );
		add_action( 'save_post', array( 'IMCPress\IMCPress', 'set_post_thumbnail' ), 17, 2 );
		if ( (int) get_option( 'disable_public_cron' ) === 1 ) {
			define( 'DISABLE_WP_CRON', true );
		}

		UI::plugins_loaded();
		Rewrite::plugins_loaded();
		Query::plugins_loaded();
		Sitemap::plugins_loaded();
		User\Avatar::plugins_loaded();
		User\Group::plugins_loaded();
		User\Registration::plugins_loaded();
		Comment\Comment::plugins_loaded();
	}

	public static function nonce_user_logged_out(): string {
		if ( isset( $_SERVER['REMOTE_ID'] ) ) {
			return wp_hash( $_SERVER['REMOTE_ID'] );
		} else {
			return wp_hash( $_SERVER['REMOTE_ADDR'] );
		}
	}

	public static function media_send_to_editor( string $html ): string {
		$host_pattern = '/' . $_SERVER['REQUEST_SCHEME'] . ':\/\/' . $_SERVER['HTTP_HOST'] . '/';
		return preg_replace( $host_pattern, '', $html );
	}

	public static function remove_header_one( array $mce_config ): array {
		$imcpress_settings = array(
			'tinymce' => array(
				'formats'            => '{' .
					'alignleft: [' .
						'{selector: "p,h2,h3,h4,h5,h6,td,th,div,ul,ol,li", styles: {textAlign:"left"}},' .
						'{selector: "img,table,dl.wp-caption", classes: "alignleft"}' .
					'],' .
					'aligncenter: [' .
						'{selector: "p,h2,h3,h4,h5,h6,td,th,div,ul,ol,li", styles: {textAlign:"center"}},' .
						'{selector: "img,table,dl.wp-caption", classes: "aligncenter"}' .
					'],' .
					'alignright: [' .
						'{selector: "p,h2,h3,h4,h5,h6,td,th,div,ul,ol,li", styles: {textAlign:"right"}},' .
						'{selector: "img,table,dl.wp-caption", classes: "alignright"}' .
					'],' .
					'strikethrough: {inline: "del"}' .
				'}',
				'remove_script_host' => true,
				'block_formats'      => 'Paragraph=p;Header 1=h2;Header 2=h3;Header 3=h4;Header 4=h5;Header 5=h6;Preformated=pre',
			),
		);
		return array_merge( $mce_config, $imcpress_settings );
	}

	public static function init(): void {
		global $wp_embed;

		remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
		remove_filter('oembed_response_data', 'get_oembed_response_data_rich', 10);
		remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
		add_filter( 'embed_oembed_discover', '__return_false', 100, 3 );
		remove_filter( 'the_content', array( $wp_embed, 'autoembed' ), 8 );
		add_filter( 'pre_do_shortcode_tag', array( __CLASS__, 'block_embed_on_private' ) , 10, 3 );

		self::load_imcpress_translations();

		require_once IMCPRESS_PATH . 'config/imcpress.php';

		Comment\Comment::set_disapproved_settings();

		$contributor_login = sanitize_key( get_option( 'contributor_login' ) );
		if ( is_user_logged_in() || ! empty( $contributor_login ) ) {
			$imcpress_post_types = self::add_author_metabox(
				$imcpress_post_types,
				$contributor_login,
			);
		}

		if ( is_admin() ) {
			$user = wp_get_current_user();
			if ( in_array( 'editor', $user->roles, true ) ) {
				add_filter(
					'wp_insert_post_data',
					array(
						'\IMCPress\IMCPress',
						'keep_modification_date',
					),
					20,
					2
				);
			}
		}

		do_action( 'setup_statuses', $imcpress_statuses );
		do_action( 'setup_taxonomies', $imcpress_taxonomies );
		do_action( 'setup_types', $imcpress_post_types );
		do_action( 'setup_contributor', $imcpress_post_types );
		do_action( 'setup_publish_page', $imcpress_post_types );
	}

	public static function load_imcpress_translations(): void {
		load_plugin_textdomain( 'imcpress', false, '/imcpress/languages' );
	}

	public static function add_author_metabox( array $post_types, string $contributor_login ): array {
		if ( is_user_logged_in() ) {
			$user = wp_get_current_user();
		} else {
			$user = get_user_by( 'login', $contributor_login );
		}
		$default = get_user_meta( $user->ID, 'nickname', true );

		$author_metabox = array(
			array(
				'slug'     => 'imcpress_author',
				'label'    => __( 'Author', 'imcpress' ),
				'position' => 'side',
				'metadata' => array(
					array(
						'slug'    => 'author_display_name',
						'label'   => __( 'Author display name', 'imcpress' ),
						'type'    => 'text',
						'default' => $default,
					),
				),
			),
		);

		foreach ( $post_types as $newtype => $options ) {
			if ( isset( $options['metaboxes'] ) ) {
				$post_types[ $newtype ]['metaboxes'] = array_merge( $author_metabox, $options['metaboxes'] );
			} else {
				$post_types[ $newtype ]['metaboxes'] = $author_metabox;
			}
		}

		\IMCPress\Type\Type::add_author_metabox_to_post( $author_metabox );

		return $post_types;
	}

	public static function setup_statuses( array $imcpress_statuses ): void {

		// Custom post status
		$custom_post_statuses = new Status\Manager();

		foreach ( $imcpress_statuses as $status ) {
			$custom_post_statuses->setup( $status );
		}
	}

	public static function setup_taxonomies( array $imcpress_taxonomies ): void {

		// Taxonomies
		$custom_taxonomies = new Taxonomy\Manager();

		foreach ( $imcpress_taxonomies as $taxonomy ) {
			$custom_taxonomies->setup( $taxonomy );
		}

		// Remove Category taxonomy, since we use Wire instead
		register_taxonomy( 'category', array() );

	}

	public static function setup_types( array $imcpress_post_types ): void {

		// Types
		$custom_post_types = new Type\Manager(
			array(
				'prefix' => 'imcpress',
			)
		);

		foreach ( $imcpress_post_types as $newtype => $options ) {
			if ( 'post' === $newtype ) {
				continue;
			}
			$default_options = array(
				'slug'         => $newtype,
				'display_page' => $newtype . 's',
			);
			$newtype_options = array_merge( $default_options, $options );
			$custom_post_types->setup( $newtype_options );
		}

	}

	public static function setup_publish_page( array $imcpress_post_types ): void {

		// Frontend publishing
		$publish_page = FrontendForm\PublishPage::get_instance( $imcpress_post_types );
		add_action( 'wp', array( $publish_page, 'generate' ) );

	}

	public static function setup_contributor( array $post_types ): void {

		$types = array_keys( $post_types );

		$contributor = new User\Contributor(
			$types,
			false
		);
		$contributor->init();
	}

	public static function set_pending_post_name( int $post_id, \WP_Post $post ): void {
		if ( 'pending' === $post->post_status && empty( $post->post_name ) ) {
			$post_name = wp_unique_post_slug(
				sanitize_title(
					$post->post_title,
					$post_id
				),
				$post_id,
				'publish',
				$post->post_type,
				$post->post_parent
			);

			global $wpdb;
			$where = array( 'ID' => $post_id );

			$wpdb->update( $wpdb->posts, array( 'post_name' => $post_name ), $where );
			clean_post_cache( $post_id );
			$wpdb->update( $wpdb->posts, array( 'guid' => get_permalink( $post_id ) ), $where );
		}
	}

	// Fired when a post statuses is set to publish (also when edited, staying piblished)
	public static function add_last_post_user_meta( $new, $old, $post ) {
		// To hook on several statuses:
		// if (in_array($new, ['publish', 'feature']))
		if ( 'publish' === $new ) {
			$prev = get_user_meta( $post->post_author, 'imcpress_last_post', true );
			if ( strtotime( $prev ) < strtotime( $post->post_date_gmt ) ) {
				update_user_meta( $post->post_author, 'imcpress_last_post', $post->post_date_gmt );
			}
		}
	}

	// Remove tags when refused or trashed
	public static function remove_tags_on_refused( string $new, string $old, \WP_Post $post ) {
		if (in_array($new, ['refused', 'trash'])) {
			wp_set_post_terms( $post->ID, '', 'post_tag' );
			wp_set_post_terms( $post->ID, '', 'place_tag' );
		}
	}

	public static function filter_hotlinks( int $id, \WP_Post $post ) {
		if ( empty( $post->post_content ) ) {
			return;
		}

		$doc = new DOMDocument();
		$doc->loadHTML( '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><div>' . $post->post_content . '</div>' );

		$xpath = new DOMXPath( $doc );
		$list  = $xpath->query( '//img | //track' );
		if ( ! $list || ! $list->length ) {
			return;
		}

		preg_match( '#(?:://)([^/]*)#', site_url(), $match );
		$url = $match[1];

		foreach ( $list as $e ) {
			$src = $e->attributes->getNamedItem( 'src' );
			if ( preg_match( sprintf( '#://(?!i%s)#', $url ), $src->textContent ) ) {
				// Remove tag
				$e->parentNode->removeChild( $e );
			}
		}

		// Render HTML without <doctype><html> stuff
		$wrapper = $doc->getElementsByTagName( 'div' )[0];
		$content = $doc->saveHTML( $wrapper );
		$content = substr( trim( $content ), 5, -6 );

		// Update post with content
		global $wpdb;
		$where = array( 'ID' => $id );

		$wpdb->update( $wpdb->posts, array( 'post_content' => $content ), $where );
		clean_post_cache( $id );
	}

	public static function filter_robots( string $output ): string {
		$block_refused_url = 'Disallow: /refused';
		return preg_replace( '#Allow: /wordpress/wp-admin/admin-ajax.php\n#', $block_refused_url, $output );
	}

	public static function set_post_thumbnail( int $post_id, \WP_Post $post ): void {
		if ( empty( $post->post_content ) ) {
			return;
		}
		
		if ( ! $thumb = get_post_thumbnail_id( $post_id ) ){
			$attached = get_attached_media( '', $post_id);
			if ( empty( $attached ) ) {
				return;
			}
			$attach_id = reset($attached)->ID;
			set_post_thumbnail( $post_id, $attach_id );
		}
	}

	public static function keep_modification_date( array $data, array $postarr ): array {
		if ( ! isset( $data['post_modified'] ) ||
			! isset( $data['post_modified_gmt'] ) ||
			! isset( $postarr['post_modified'] ) ||
			! isset( $postarr['post_modified_gmt'] ) ) {
			return $data;
		}

		if ( ! $postarr['ID'] || (int) $postarr['ID'] === 0 ) {
			return $data;
		}

		$user = wp_get_current_user();
		$post = get_post( $postarr['ID'] );

		if ( (int) $user->ID !== (int) $post->post_author ) {
			$data['post_date']         = $post->post_date;
			$data['post_date_gmt']     = $post->post_date_gmt;
			$data['post_modified']     = $post->post_modified;
			$data['post_modified_gmt'] = $post->post_modified_gmt;
		}

		return $data;
	}

	public static function block_embed_on_private( $ret, $tag, $attr ) {
		if ( $tag === 'embed' ) {
			return '';
		}
		return $ret;
	}

	public static function add_feature_meta( string $new, string $old, \WP_Post $post ): void {
		if ( $new === 'feature' ) {
			update_post_meta( $post->ID, 'imcpress_feature_date', wp_date( 'c' ) );
		}
    }

	public static function imcp_author( $display_name ) {
		if ( is_group( get_the_author_meta( 'ID' ) ) )
		{
			return  $display_name;
		}

		$post = get_post( get_the_ID() );
		$author = get_post_meta( $post->ID, $post->post_type . '_metadata_author_display_name', true);
		
		// In admin view, display meta_author AND actual user name
		if ( is_admin() && $display_name !== $author )
		{
			return sprintf( '%s (%s)', $author, $display_name );
		}
		return $author;
	}

	public static function rss_tags( $terms ) {
		$terms = array();
		foreach ( get_taxonomies() as $tax )
		{
			$t = get_the_terms( 0, $tax );
			if ( ! is_array( $t) )
			{
				continue;
			}
			$terms = array_merge( $terms, $t );
		}
		return $terms;
	}

	// Deactivation hook
	public static function deactivate(): void {
		\IMCPress\User\Contributor::remove_caps( array( 'upload_files' ), 'contributor' );
		flush_rewrite_rules();
	}

	public static function activate(): void {
		\IMCPress\Option\Manager::activate();
		\IMCPress\User\Contributor::add_caps( array( 'upload_files' ), 'contributor' );
		flush_rewrite_rules();
	}
}

<?php
namespace IMCPress\Type;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Messages {

	public function __construct() {}

	public static function generate_type_messages( array $messages ): array {

		global $post;
		if ( ! empty( $post ) ) {

			$permalink = get_permalink( $post );

			$messages[ $post->post_type ] = array(
				0  => '',
				1  => sprintf(
					__(
						ucfirst( $post->post_type ) . 's updated. <a target="_blank" href="%s">View ' . $post->post_type . '</a>',
						$post->post_type
					),
					esc_url( $permalink )
				),
				2  => __( 'Custom field updated.', $post->post_type ),
				3  => __( 'Custom field deleted.', $post->post_type ),
				4  => __( ucfirst( $post->post_type ) . 's updated.', $post->post_type ),
				/* translators: %s: date and time of the revision */
				5  => isset( $_GET['revision'] ) ? sprintf(
					__(
						ucfirst( $post->post_type ) . 's restored to revision from %s',
						$post->post_type
					),
					wp_post_revision_title(
						(int) $_GET['revision'],
						false
					)
				) : false,
				6  => sprintf(
					__(
						ucfirst( $post->post_type ) . 's published. <a href="%s">View ' . $post->post_type . '</a>',
						$post->post_type
					),
					esc_url( $permalink )
				),
				7  => __( ucfirst( $post->post_type ) . 's saved.', $post->post_type ),
				8  => sprintf(
					__(
						ucfirst( $post->post_type ) . 's submitted. <a target="_blank" href="%s">Preview ' . $post->post_type . '</a>',
						$post->post_type
					),
					esc_url( add_query_arg( 'preview', 'true', $permalink ) )
				),
				9  => sprintf(
					__(
						ucfirst( $post->post_type ) . 's scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview ' . $post->post_type . '</a>',
						$post->post_type
					),
					date_i18n(
						__(
							'M j, Y @ G:i',
							$post->post_type
						),
						strtotime(
							$post->post_date
						)
					),
					esc_url( $permalink )
				),
				10 => sprintf(
					__(
						ucfirst( $post->post_type ) . 's draft updated. <a target="_blank" href="%s">Preview ' . $post->post_type . '</a>',
						$post->post_type
					),
					esc_url( add_query_arg( 'preview', 'true', $permalink ) )
				),
			);
		}

		return $messages;
	}

	public static function generate_type_bulk_messages( array $bulk_messages, array $bulk_counts ): array {
		global $post;
		if ( ! empty( $post ) ) {

			$bulk_messages[ $post->post_type ] = array(
				'updated'   => _n(
					'%s ' . $post->post_type . ' updated.',
					'%s ' . $post->post_type . 's updated.',
					$bulk_counts['updated'],
					$post->post_type
				),
				'locked'    => ( 1 === $bulk_counts['locked'] ) ? __(
					'1 ' . $post->post_type . ' not updated, somebody is editing it.',
					$post->post_type
				) :
					_n(
						'%s ' . $post->post_type . ' not updated, somebody is editing it.',
						'%s ' . $post->post_type . 's not updated, somebody is editing them.',
						$bulk_counts['locked'],
						$post->post_type
					),
				'deleted'   => _n(
					'%s ' . $post->post_type . ' permanently deleted.',
					'%s ' . $post->post_type . 's permanently deleted.',
					$bulk_counts['deleted'],
					$post->post_type
				),
				'trashed'   => _n(
					'%s ' . $post->post_type . ' moved to the Trash.',
					'%s ' . $post->post_type . 's moved to the Trash.',
					$bulk_counts['trashed'],
					$post->post_type
				),
				'untrashed' => _n(
					'%s ' . $post->post_type . ' restored from the Trash.',
					'%s ' . $post->post_type . 's restored from the Trash.',
					$bulk_counts['untrashed'],
					$post->post_type
				),
			);
		}

		return $bulk_messages;
	}

}

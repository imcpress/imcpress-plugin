<?php

namespace IMCPress\Type;

use IMCPress\AbstractManager;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Manager extends AbstractManager {

	public string $prefix;
	public array $slugs = array();

	public function __construct( array $init_data ) {
		$this->prefix = $init_data['prefix'];
	}

	public function setup( array $options ): void {
		$options['prefix'] = $this->prefix;
		$ctp = new Type( $options );
		array_push( $this->slugs, $ctp->full_slug() );

	}

}

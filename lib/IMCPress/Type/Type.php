<?php

namespace IMCPress\Type;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

/* Custom Post Type */
class Type {

	public string $prefix;
	public string $slug;
	public array $taxonomy_slug;
	public array $supports;
	public ?array $metaboxes;
	public ?array $metadata;
	public ?int $menu_position;
	public ?string $menu_icon;
	public bool $hierarchical;
	public ?array $rewrite;
	public string $query_var;
	public string $label;
	private array $capabilities;

	public function __construct( $init_data ) {

		$this->prefix        = $init_data['prefix'];
		$this->slug          = $init_data['slug'];
		$this->taxonomy_slug = $init_data['taxonomy_slug'];
		$this->supports      = $init_data['supports'] ?? array( 'title', 'editor', 'author' );
		$this->metaboxes     = $init_data['metaboxes'] ?? null;
		$this->metadata      = $init_data['metadata'] ?? null;
		$this->menu_position = $init_data['menu_position'] ?? null;
		$this->menu_icon     = $init_data['menu_icon'] ?? null;
		$this->hierarchical  = $init_data['hierarchical'] ?? false;
		$this->rewrite       = $init_data['rewrite'] ?? null;
		$this->query_var     = $init_data['query_var'] ?? $this->slug;
		$this->label         = $init_data['label'] ?? $this->slug;

		$this->capabilities = $this->build_capabilities();

		add_action( 'init', array( $this, 'create_ctp' ) );
		add_action( 'save_post_' . $this->full_slug(), array( $this, 'save_ctp_custom_metadata' ), 11, 3 );

		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_datetimepicker' ) );
		add_filter( 'post_updated_messages', array( Messages::class, 'generate_type_messages' ) );
		add_filter( 'bulk_post_updated_messages', array( Messages::class, 'generate_type_bulk_messages' ), 10, 2 );

	}

	public static function enqueue_datetimepicker() {
		wp_enqueue_style( 'datetimepicker', IMCPRESS_URL . 'assets/css/jquery.datetimepicker.min.css' );
		wp_enqueue_script( 'datetimepicker', IMCPRESS_URL . 'assets/js/jquery.datetimepicker.full.min.js' );
	}

	private function build_capabilities() {
		return array(
			'publish_posts'        => 'publish_posts',
			'edit_posts'           => 'edit_posts',
			'edit_others_posts'    => 'edit_others_posts',
			'edit_published_posts' => 'edit_published_posts',
			'delete_posts'         => 'delete_posts',
			'delete_others_posts'  => 'delete_others_posts',
			'read_private_posts'   => 'read_private_posts',
			'edit_post'            => 'edit_post',
			'delete_post'          => 'delete_post',
			'read_post'            => 'read_post',
		);
	}

	public function full_slug() {
		return strtolower( $this->prefix . '_' . $this->slug );
	}

	public function create_ctp() {

		$post_type_slug = $this->full_slug();

		if ( ! in_array( 'author', $this->supports, true ) ) {
			$this->supports[] = 'author';
		}

		$lc_label = mb_strtolower( $this->label );

		$args = array(
			'labels'               => array(
				'name'          => ucfirst( $this->label ) . 's',
				'singular_name' => ucfirst( $this->label ),
				'all_items'     => __( 'All', 'imcpress' ),
				'add_new'       => sprintf( __( 'Add a New %s', 'imcpress' ), $lc_label ),
				'add_new_item'  => sprintf( __( 'Add a New %s', 'imcpress' ), $lc_label ),
				'edit_item'     => sprintf( __( 'Edit a %s', 'imcpress' ), $lc_label ),
				'new_item'      => sprintf( __( 'Add a New %s', 'imcpress' ), $lc_label ),
				'view_item'     => sprintf( __( 'View %s', 'imcpress' ), $lc_label ),
				'view_items'     => sprintf( __( 'View %s', 'imcpress' ), $lc_label ),
				'search_items'  => sprintf( __( 'Search a %s', 'imcpress' ), $lc_label ),
			),
			'hierarchical'         => $this->hierarchical,
			'public'               => true,
			'show_in_rest'         => false,
			'has_archive'          => true,
			'supports'             => $this->supports,
			'menu_position'        => $this->menu_position,
			'menu_icon'            => $this->menu_icon,
			'query_var'            => $this->query_var,
			'rewrite'              => $this->rewrite ?? array(
				'slug' => $this->slug . 's',
			),
			'register_meta_box_cb' => array( $this, 'create_ctp_custom_metaboxes' ),
			'map_meta_cap'         => true,
		);

		$args['rewrite']['with_front'] = false;

		$args['capabilities'] = $this->capabilities;

		register_post_type( $post_type_slug, $args );
		// Declare actual permastruct here, or do we consider it's a customizable theme thing?
		// Yes, that'd be the most relevant place I think

		if ( $this->taxonomy_slug ) {
			foreach ( $this->taxonomy_slug as $tax ) {
				register_taxonomy_for_object_type( $tax, $post_type_slug );
			}
		}
	}

	public function create_ctp_custom_metaboxes( $post ) {

		$post_type_slug = get_post_type( $post );

		if ( null !== $this->metaboxes ) {

			foreach ( $this->metaboxes as $metabox ) {

				$metabox_id       = $post_type_slug . '_metabox_' . $metabox['slug'];
				$metabox_label    = $metabox['label'];
				$metabox_callback = array( $this, 'create_ctp_custom_metadata' );
				$metabox_screen   = $post_type_slug;
				$metabox_content  = $metabox['position'];
				$metabox_priority = 'high';
				$metabox_cb_args  = array( $metabox['metadata'], $post_type_slug );

				add_meta_box(
					$metabox_id,
					$metabox_label,
					$metabox_callback,
					$metabox_screen,
					$metabox_content,
					$metabox_priority,
					$metabox_cb_args
				);

				foreach ( $metabox['metadata'] as $metadata ) {
					register_post_meta(
						$post_type_slug,
						$post_type_slug . '_metadata_' . $metadata['slug'],
						array(
							'single'       => true,
							'show_in_rest' => false,
						)
					);
				}
			}
		}
	}

	public static function create_ctp_custom_metadata( \WP_Post $post, array $data ) {
		$metadata       = $data['args'][0];
		$post_type_slug = sanitize_key( $data['args'][1] );

		$html = '';

		foreach ( $metadata as $metadatum ) {

			$html .= '<div class="metadata-wrap">';

			$metadatum_id      = $post_type_slug . '_metadata_' . sanitize_key( $metadatum['slug'] );
			$metadatum_value   = get_post_meta( $post->ID, $metadatum_id, true );
			$metadatum_default = $metadatum['default'] ?? '';
			$metadatum_value   = $metadatum_value ? $metadatum_value : $metadatum_default;

			$metabox = new \IMCPress\Metabox\Display( $metadatum_id, $metadatum_value, $metadatum );
			$html .= $metabox->html;

			$html .= '</div>';

			$meta_nonce = new \IMCPress\Metabox\Display(
				$metadatum_id . '_nonce',
				wp_create_nonce( $metadatum_id ),
				array(
					'type' => 'hidden',
				),
			);
			$html .= $meta_nonce->html;
		}
		echo $html;
	}

	public static function save_ctp_custom_metadata( int $post_id, \WP_Post $post, bool $update = true ): void {

		if ( ! self::is_valid_request( $post ) ) {
			return;
		}
		$post_type_slug = get_post_type( $post );
		$metaboxes = get_post_type_object( $post_type_slug )->register_meta_box_cb[0]->metaboxes;
		if ( null !== $metaboxes ) {
			foreach ( $metaboxes as $metabox ) {
				foreach ( $metabox['metadata'] as $metadatum ) {
					$metadata_id = sanitize_key( $post_type_slug . '_metadata_' . $metadatum['slug'] );
					if ( empty( $_POST[ $metadata_id . '_nonce' ] ) ) {
						return;
					}
					if ( ! wp_verify_nonce( sanitize_key( $_POST[ $metadata_id . '_nonce' ] ), $metadata_id ) ) {
						return;
					}
					$sanitizer = new \IMCPress\Metabox\Sanitize( sanitize_key( $metadatum['type'] ) );
					$value = $sanitizer->sanitize( wp_unslash( $_POST[ $metadata_id ] ) );
					if ( empty( $value ) ) {
						delete_post_meta( $post->ID, $metadata_id );
					} else {
						update_post_meta( $post->ID, $metadata_id, $value );
					}
				}
			}
		}
	}

	private static function is_valid_request( \WP_Post $post ): bool {

		if ( ! isset( $_POST ) ) {
			return false;
		}
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return false;
		}
		if ( is_user_logged_in() ) {
			if ( ! current_user_can( 'edit_post', $post->ID ) ) {
				return false;
			}
			if ( is_admin() && 'revision' === $post->post_type ) {
				return false;
			}
		}
		return true;
	}

	public static function add_author_metabox_to_post( array $author_metabox ): void {

		register_post_meta(
			'post',
			'post_metadata_author_display_name',
			array(
				'single'       => false,
				'show_in_rest' => false,
			),
		);
		add_action(
			'save_post',
			array(
				'\IMCPress\Type\Type',
				'save_post_author_metadata',
			),
			11,
			3,
		);
		add_action(
			'add_meta_boxes',
			function() use ( $author_metabox ) {
				add_meta_box(
					'post_metadata_author_display_name',
					$author_metabox[0]['label'],
					array( '\IMCPress\Type\Type', 'create_ctp_custom_metadata' ),
					'post',
					$author_metabox[0]['position'],
					'high',
					array( $author_metabox[0]['metadata'], 'post' ),
				);
			}
		);
	}

	public static function save_post_author_metadata( int $post_id, \WP_Post $post, bool $update = true ): void {
		if ( ! self::is_valid_request( $post ) ) {
			return;
		}
		$metadata_id = 'post_metadata_author_display_name';
		if ( empty( $_POST[ $metadata_id . '_nonce' ] ) ) {
			return;
		}
		if ( ! wp_verify_nonce( sanitize_key( $_POST[ $metadata_id . '_nonce' ] ), $metadata_id ) ) {
			return;
		}
		$sanitizer = new \IMCPress\Metabox\Sanitize( 'text' );
		$value = $sanitizer->sanitize( wp_unslash( $_POST[ $metadata_id ] ) );
		if ( empty( $value ) ) {
			delete_post_meta( $post->ID, $metadata_id );
		} else {
			update_post_meta( $post->ID, $metadata_id, $value );
		}

	}
}

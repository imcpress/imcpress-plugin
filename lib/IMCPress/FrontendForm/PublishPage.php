<?php

namespace IMCPress\FrontendForm;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

require_once ABSPATH . 'wp-admin/includes/post.php';

class PublishPage {

	public string $publish_page;
	public string $contributor_login;
	public array $post_types;
	public \WP_Post $post;
	public string $type;
	private static $instance = null;

	public function __construct( array $imcpress_types ) {
		$this->publish_page = sanitize_key( get_option( 'publish_page_slug' ) );
		$this->contributor_login = sanitize_key( get_option( 'contributor_login' ) );
		add_filter( 'query_vars', array( $this, 'add_post_type_query_var' ) );
		add_action( 'generate_rewrite_rules', array( $this, 'add_publish_page_rewrite_rule' ) );
		if ( empty( $this->contributor_login ) ) {
			return;
		}
		$this->post_types = $imcpress_types;
		add_action( 'wp_ajax_frontend_taxonomy_autocomplete', array( $this, 'frontend_taxonomy_autocomplete' ) );
		add_action( 'wp_ajax_nopriv_frontend_taxonomy_autocomplete', array( $this, 'frontend_taxonomy_autocomplete' ) );
		add_action( 'wp_ajax_nopriv_send-attachment-to-editor', array( $this, 'send_attachment_to_editor' ) );

		add_filter( 'publish_description', array( $this, 'publish_description' ), 5, 2 );
		add_filter( 'publish_types_descriptions', array( $this, 'publish_types_descriptions' ), 5, 3 );
	}

	public static function get_instance( array $init_data = array() ) {
		if ( self::$instance == null ) {
			self::$instance = new self( $init_data );
		}

		return self::$instance;
	}

	public function generate(): void {
		$pagename = esc_attr( strval( get_query_var( 'pagename' ) ) );
		if ( $pagename === $this->publish_page ) {
			if ( empty( $this->contributor_login ) ) {
				$imcpress_options = \IMCPress\Option\IMCPress::instance()->get_options();
				wp_die(
					sprintf(
						__(
							'Please configure the imcpress %s option to enable frontend posting.',
							'imcpress',
						),
						strtolower( $imcpress_options['contributor_login']['label'] ),
					),
				);
			}
			add_filter( 'template_include', array( $this, 'publish_page_template' ) );
			add_filter( 'wp_title', array( $this, 'fix_publish_page_title' ), 10, 0 );
			if ( get_query_var( 'type' ) ) {
				$this->type = sanitize_key( strval( get_query_var( 'type' ) ) );
				if ( ! array_key_exists( $this->type, $this->post_types ) ) {
					wp_safe_redirect( get_home_url() . '/' . $this->publish_page );
				}
				$imcpress_type = 'post' === $this->type ? $this->type : 'imcpress_' . $this->type;
				if ( isset( $_SERVER['REQUEST_METHOD'] ) && 'POST' === $_SERVER['REQUEST_METHOD'] &&
					isset( $_REQUEST['post_ID'] ) && ! empty( $_REQUEST['post_ID'] ) &&
					PostTypeFormValidator::valid_post_id( (int) $_REQUEST['post_ID'] ) ) {
					$this->post = get_post( (int) $_REQUEST['post_ID'] );
				} elseif ( isset( $_SERVER['REQUEST_METHOD'] ) && 'GET' === $_SERVER['REQUEST_METHOD'] ) {
					$this->post = get_default_post_to_edit( $imcpress_type, true );
				}
				if ( ! isset( $this->post ) ) {
					wp_die( __( 'Sorry, you are not allowed to do that.' ) );
				}
				add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
				add_action( 'wp_footer', array( $this, 'display_wp_editor' ), 100 );
				if ( ! is_user_logged_in() ) {
					add_filter( 'plupload_default_settings', array( $this, 'plupload_default_settings' ) );
				}
				$form = $this->generate_form();
				$this->add_post_type_form_shortcodes( $form );
			}
		}
	}

	public function plupload_default_settings( $settings ): array {
		$settings['url'] = IMCPRESS_URL . 'async-upload.php';
		return $settings;
	}

	public function send_attachment_to_editor() {
		wp_ajax_send_attachment_to_editor();
	}

	public function enqueue_scripts(): void {
		global $pagename;
		if ( $pagename === $this->publish_page ) {
			wp_enqueue_editor();
			wp_enqueue_media( array( 'post' => $this->post ) );
			foreach ( array( 'common', 'edit', 'forms' ) as $style ) {
				wp_enqueue_style( $style );
			}
			wp_enqueue_script(
				'taxonomy_autocomplete',
				IMCPRESS_URL . 'assets/js/taxonomy_autocomplete.js',
				array( 'jquery-ui-autocomplete' ),
				null,
				true,
			);
			$ajax_vars = array(
				'ajax_url'   => admin_url( 'admin-ajax.php' ),
				'ajax_nonce' => wp_create_nonce( 'taxonomy_autocomplete' ),
			);
			wp_localize_script(
				'taxonomy_autocomplete',
				'imcpress',
				$ajax_vars,
			);
			wp_enqueue_style(
				'datetimepicker',
				IMCPRESS_URL . 'assets/css/jquery.datetimepicker.min.css',
			);
			wp_enqueue_script(
				'datetimepicker',
				IMCPRESS_URL . 'assets/js/jquery.datetimepicker.full.min.js',
			);
		}
	}

	public function display_wp_editor(): void {
		global $pagename;
		if ( $pagename === $this->publish_page ) {
			include IMCPRESS_PATH . 'assets/js/wpeditor.js';
		}
	}

	public function add_publish_page_rewrite_rule( object $wp_rewrite ): void {
		$publish_page_rule = array(
			// Split rule to not match /published (status) into this
			$this->publish_page.'/?$' => 'index.php?pagename=' . $this->publish_page,
			$this->publish_page.'/([^/]+)/?$' => 'index.php?pagename=' . $this->publish_page . '&type=$matches[1]',
		);

		$wp_rewrite->rules = array_merge(
			$publish_page_rule,
			$wp_rewrite->rules
		);
	}

	public function add_post_type_query_var( array $vars ): array {
		$vars[] = 'type';
		return $vars;
	}

	public function publish_page_template( string $template ): string {
		$pagename = esc_attr( strval( get_query_var( 'pagename' ) ) );

		if ( $pagename === $this->publish_page ) {
			status_header( '200' );
			$template = IMCPRESS_PATH . 'templates/publish.php';
		}
		return $template;
	}

	private function generate_form(): PostTypeFormGenerator {

		$type_options        = $this->post_types[ $this->type ];
		$post_type_metaboxes = $type_options['metaboxes'] ?? array();
		$captcha_prefix      = self::generate_captcha();
		$post_type_label     = $type_options['label'] ?? $this->type;

		$post_type_form = new PostTypeFormGenerator(
			$this->type,
			$this->publish_page,
			$type_options['supports'],
			$post_type_metaboxes,
			$type_options['taxonomy_slug'],
			$captcha_prefix,
			$this->post,
			$post_type_label,
		);

		return $post_type_form;
	}

	private function add_post_type_form_shortcodes( PostTypeFormGenerator $form ): void {
		add_shortcode( $this->type . '_form', array( $form, 'display_form' ) );
	}

	public static function frontend_taxonomy_autocomplete() {
		$taxonomy = sanitize_key( wp_unslash( $_GET['taxonomy'] ) );
		$term     = sanitize_key( wp_unslash( $_GET['term'] ) );
		$nonce    = sanitize_key( wp_unslash( $_GET['nonce'] ) );
		if ( ! wp_verify_nonce( $nonce, 'taxonomy_autocomplete' ) ) {
			wp_die( '0' );
		}
		$results = get_terms(
			array(
				'taxonomy'   => $taxonomy,
				'name__like' => $term,
				'fields'     => 'names',
				'hide_empty' => false,
			)
		);
		echo implode( "\n", $results );
		wp_die();
	}

	public static function generate_captcha(): string {
		$captcha  = new Captcha();
		$word     = $captcha->generate_random_word();
		$filename = wp_rand();
		$captcha->generate_image( $filename, $word );
		return $filename;
	}

	public static function fix_publish_page_title(): string {
		return __( 'Publish your content', 'imcpress' );
	}

	private static function get_desc_page( string $key, string $content = null ) {
		// If page doesn't exist, create it
		$get_page = new \WP_Query( array(
			'post_type'              => 'page',
			'title'                  => $key,
			'post_status'            => 'all',
			'posts_per_page'         => 1,
			'no_found_rows'          => true,
			) );
		$page = $get_page->posts[0];
		if ( empty( $page ) ) {
			$default_content = $content ?? sprintf( __( 'Post your %s', 'imcpress' ), $key );
			$page = array(
				'post_type'    => 'page',
				'post_title'   => $key,
				'post_content' => $default_content,
				'post_status'  => 'private',
				// Fix: set author_id?
			);

			$id = wp_insert_post( $page );

			if ( 0 != $id ) {
				$page = get_post( $id );
			}
		}

		$page->post_content = apply_filters( 'the_content', $page->post_content );
		$page->post_content = str_replace( ']]>', ']]&gt;', $page->post_content );

		return $page;
	}

	public function display_descriptions(): void {

		$pages = array();

		echo apply_filters( 'publish_description', '', self::get_desc_page( $this->publish_page, __( 'Choose what you want to publish:' ) ) );

		foreach ( $this->post_types as $key => $val ) {
			$pages[ $key ]        = self::get_desc_page( $key );
			$pages[ $key ]->label = $val['label'];
		}

		echo apply_filters( 'publish_types_descriptions', '', $pages, $this->publish_page );
	}

	public static function get_page_description( string $key ) {
		$page = self::get_desc_page( $key );
		return $page->post_content;
	}

	public static function publish_types_descriptions( string $content, array $pages, string $publish_page ): string {
		$content = '<ul class"publish-container">';
		foreach ( $pages as $key => $page ) {
			$content .= '<div class="publish-description">';
			$content .= sprintf(
				'<li><a href="%s">%s</a></li>',
				home_url(
					sprintf(
						'%s/%s',
						$publish_page,
						$key
					)
				),
				$page->post_content
			);
			$content .= '</div>';
		}
		$content .= '</ul>';
		return $content;
	}

	public static function publish_description( string $content, $page ): string {
		$text = apply_filters( 'the_content', $page->post_content );
		$text = str_replace( ']]>', ']]&gt;', $text );
		return sprintf( '<div class"publish-publish">%s</div>', $text );
	}
}

<?php

namespace IMCPress\FrontendForm;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class PostTypeFormElement {

	public function __construct() {}

	public static function post_id( int $id ):string {
		$postid_metabox = new \IMCPress\Metabox\Display(
			'post_ID',
			$id,
			array(
				'type'  => 'hidden',
			),
		);
		return $postid_metabox->html;
	}

	public static function title(): string {
		$current_metabox = new \IMCPress\Metabox\Display(
			'title',
			'',
			array(
				'type'  => 'text',
				'label' => __( 'Title'),
			),
			array(
				'input_name'       => 'post_title',
				'input_attributes' => ' spellcheck="true" autocomplete="off" ',
			),
		);

		$html  = '<div id="post_title" class="postbox">';
		$html .= '<div class="inside">';
		$html .= $current_metabox->html;
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	public static function taxonomy( string $taxonomy, \WP_Post $post ): string {
		if ( ! taxonomy_exists( $taxonomy ) ) {
			return '<b>Wrong taxonomy</b>';
		}

		$tax = get_taxonomy( $taxonomy );

		$html  = '<div id="' . $tax->name . 'div" class="postbox">';
		$html .= '<div class="inside">';

		if ( is_array( $tax->meta_box_cb ) ) {
			$html .= self::custom_tax_metabox( $taxonomy, $tax->meta_box_cb, $post );
		} else {
			$html .= self::tax_metabox( $tax );
		}

		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	public static function custom_tax_metabox( string $tax_name, array $metabox, \WP_Post $post ): string {
		$box = array(
			'id'   => $tax_name,
			'args' => array(
				'taxonomy' => $tax_name,
			),
		);

		ob_start();
		call_user_func( $metabox, $post, $box );
		$html = ob_get_clean();

		return $html;
	}

	public static function tax_metabox( \WP_Taxonomy $taxonomy ): string {
		$html = '';
		$html .= \IMCPress\Metabox\Display::taxonomy_autocomplete( $taxonomy );

		return $html;
	}


	public static function content(): string {
		$current_metabox = new \IMCPress\Metabox\Display(
			'content',
			'',
			array(
				'type'  => 'textarea',
				'label' => '',
			),
			array(
				'input_name'       => 'post_content',
				'input_attributes' => ' rows="35" cols="80" spellcheck="true" autocomplete="off" ',
				'required'			=> false
			),
		);
		$html =	'<div id="contentwrap">';
		$html .= $current_metabox->html;
		$html .= '</textarea>';
		$html .= '</div>';
		return $html;
	}

	public static function captcha( string $captcha_prefix ): string {
		$captcha_metabox = new \IMCPress\Metabox\Display(
			'captcha',
			'',
			array(
				'type'  => 'text',
				'label' => __('Enter what\'s in the in the picture above:', 'imcpress'),
			),
			array(
				'input_name'       => 'captcha',
				'input_attributes' => ' spellcheck="false" autocomplete="off" ',
			),
		);
		$prefix = new \IMCPress\Metabox\Display(
			'prefix',
			$captcha_prefix,
			array(
				'type'  => 'hidden',
			),
		);

		$html = '<div id="captcha-container" class="postbox">';
		$html .= '<div class="inside">';
		$html .= '<p class="attribute-wrapper"><label class="post-attributes-label" for="captcha">' .
			__('We don\'t allow publication from bots', 'imcpress') .
			'</label></p>';
		$html .= '<img src="' . WP_CONTENT_URL . '/tmp/' . $captcha_prefix . '.png" alt="captcha" align="middle" />';
		$html .= $captcha_metabox->html;
		$html .= $prefix->html;
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	public static function nonce( string $pagename, string $type, string $post_id): string {
		$nonce_metabox = new \IMCPress\Metabox\Display(
			'nonce',
			wp_create_nonce( $pagename . $type . $post_id ),
			array(
				'type' => 'hidden',
			),
		);
		return $nonce_metabox->html;
	}

	public static function submit(): string {
		$html = '<p class="attribute-wrapper">';
		$html .= '<div class="d-grid mx-auto">';
		$html .= '<input type="submit" name="publish" id="publish" class="btn btn-primary btn-lg" value="Publier">';
		$html .= '</div>';
		$html .= '</p>';
		return $html;
	}
}

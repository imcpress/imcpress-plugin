<?php

namespace IMCPress\FrontendForm;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class PostTypeFormValidator {

	private array $request;
	private string $type;
	private string $imcpress_post_type;
	private array $post_type_options;
	private array $newpost_args;
	private string $contributor;


	public function __construct( array $request, string $type ) {
		$this->request = $request;
		$this->type = $type;
		$this->imcpress_post_type = 'post' === $type ? $type : 'imcpress_' . $type;
		$this->newpost_args = array(
			'post_status'    => 'pending',
			'comment_status' => 'open',
			'ping_status'    => 'closed',
		);
		$this->contributor_login = sanitize_key( get_option( 'contributor_login' ) );

	}

	public function save(): array {

		$this->set_imcpress_options();

		$errors = $this->check_request();
		if ( ! empty( $errors ) ) {
			return $errors;
		}

		if ( is_user_logged_in() ) {
			$user = wp_get_current_user();
		} else {
			$user = wp_set_current_user( null, $this->contributor_login );
		}

		$this->newpost_args['ID'] = (int) $this->request['post_ID'];
		$this->newpost_args['post_type'] = (string) $this->imcpress_post_type;
		if ( in_array( 'title', $this->post_type_options['supports'], true ) ) {
			$this->newpost_args['post_title'] = (string) wp_strip_all_tags( $this->request['post_title'] );
		}
		$this->newpost_args['post_content'] = (string) $this->request['post_content'];
		$this->newpost_args['post_author'] = $user->ID;
		$this->newpost_args['tax_input'] = (array) $this->request['tax_input'];
		$this->set_meta_input();

		$post_id = wp_insert_post( $this->newpost_args );
		if ( ! $post_id instanceof \WP_Error ) {
			return array( 'id' => $post_id );
		} else {
			return array(
				'name'    => 'WP_Error',
				'message' => __( 'A WP error occured', 'imcpress' ),
			);
		}
	}

	private function check_request(): array {
		$errors = array();

		if ( ! $this->valid_post_id( (int) $this->request['post_ID'] ) ) {
			wp_die( __( 'Sorry, you are not allowed to do that.' ) );
		}

		$imcpress_types = get_post_types();
		if ( ! in_array( $this->imcpress_post_type, $imcpress_types, true ) ) {
			$errors[] = array(
				'name'    => 'TypeError',
				'message' => __( 'Invalid post type.', 'imcpress' ),
			);
		}

		if ( isset( $this->post_type_options['require_attachment'] ) && true === $this->post_type_options['require_attachment'] ) {
			$attached = get_attached_media( '', $this->request['post_ID'] );
			if ( empty( $attached ) ) {
				$errors[] = array(
					'name'    => 'AttachmentError',
					'message' => __( 'This type require (at least) one attachment, none given.', 'imcpress' ),
				);
			}
		}

		if ( in_array( 'title', $this->post_type_options['supports'], true ) ) {
			if ( ! $this->check_isset( 'post_title' ) ) {
				$errors[] = array(
					'name'    => 'EmptyError',
					'message' => __( 'Title' ) . __( ' is empty', 'imcpress' ),
				);
			}
			if ( str_word_count( $this->request['post_title'] ) < 3 ) {
				$errors[] = array(
					'name'    => 'LengthError',
					'message' => __( 'Title' ) . sprintf( __( ' is too short, it should be at least %d words', 'imcpress' ), 3 ),
				);
			}
		}

		if ( ! $this->check_isset( 'post_content' ) ) {
			$errors[] = array(
				'name'    => 'EmptyError',
				'message' => __( 'Content' ) . __( ' is empty', 'imcpress' ),
			);
		}

		if ( $this->type === 'tumble' ) {
			$min_length = 10;

			if ( str_word_count( $this->request['post_content'] ) > 150 ) {
				$errors[] = array(
					'name'    => 'LengthError',
					'message' => __( 'Content' ) . sprintf( __( ' is too long, it should be max %d words', 'imcpress' ), 150 ),
				);
			}
		} else {
			$min_length = 100;
		}

		if ( str_word_count( $this->request['post_content'] ) < $min_length ) {
			$errors[] = array(
				'name'    => 'LengthError',
				'message' => __( 'Content' ) . sprintf( __( ' is too short, it should be at least %d words', 'imcpress' ), $min_length ),
			);
		}

		foreach ( $this->post_type_options['taxonomy_slug'] as $taxonomy ) {
			if ( ! $this->check_isset( '', array( 'tax_input', $taxonomy ) ) ) {
				$tax_labels = get_taxonomy_labels( $taxonomy );
				$errors[] = array(
					'name'    => 'EmptyError',
					'message' => sprintf( __( '%s is empty', 'imcpress' ), $tax_labels->name ),
				);
			}
		}

		foreach ( $this->post_type_options['metaboxes'] as $metabox ) {
			foreach ( $metabox['metadata'] as $metadata => $metadatum ) {
				if ( ! $this->check_isset( $this->imcpress_post_type . '_metadata_' . $metadatum['slug'] ) ) {
					$errors[] = array(
						'name'    => 'EmptyError',
						'message' => $metabox['label'] . __( ' is empty', 'imcpress' ),
					);
				}
			}
		}

		return $errors;
	}

	private function check_isset( string $field = '', array $fields = array() ): bool {
		if ( ! empty( $field ) ) {
			if ( ! isset( $this->request[ $field ] ) || empty( $this->request[ $field ] ) ) {
				return false;
			}
		} else if ( ! empty( $fields ) ) {
			if ( ! isset( $this->request[ $fields[0] ][ $fields[1] ] ) || empty( $this->request[ $fields[0] ][ $fields[1] ] ) ) {
				return false;
			}
		}
		return true;
	}

	private function set_meta_input(): void {
		foreach ( $this->post_type_options['metaboxes'] as $metabox ) {
			foreach ( $metabox['metadata'] as $metadata => $metadatum ) {
				$field = $this->imcpress_post_type . '_metadata_' . sanitize_key( $metadatum['slug'] );
				$sanitizer = new \IMCPress\Metabox\Sanitize( sanitize_key( $metadatum['type'] ) );
				$this->newpost_args['meta_input'][ $field ] = $sanitizer->sanitize( $this->request[ $field ] );
			}
		}
	}

	private function set_imcpress_options(): void {
		require IMCPRESS_PATH . 'config/imcpress.php';

		$imcpress_post_types = \IMCPress\IMCPress::add_author_metabox(
			$imcpress_post_types,
			$this->contributor_login
		);
		$this->post_type_options = $imcpress_post_types[ $this->type ];
	}

	public static function check_captcha( array $request ): bool {
		if ( is_user_logged_in() ) {
			return true;
		}
		if ( ! isset( $request['prefix'] ) || empty( $request['prefix'] ) ) {
			return false;
		}
		if ( ! isset( $request['captcha'] ) || empty( $request['captcha'] ) ) {
			return false;
		}
		$prefix = sanitize_key( wp_unslash( $request['prefix'] ) );
		$response = sanitize_key( wp_unslash( $request['captcha'] ) );
		$captcha = new Captcha();
		if ( $captcha->check( $prefix, $response ) ) {
			return true;
		}
		return false;
	}

	public static function check_nonce( array $request, string $type ): bool {
		if ( ! isset( $request['nonce'] ) || empty( $request['nonce'] ) || ! isset( $request['post_ID'] ) ) {
			return false;
		}
		global $pagename;
		$response = sanitize_key( wp_unslash( $request['nonce'] ) );
		$type = 'post' === $type ? $type : 'imcpress_' . $type;
		if ( wp_verify_nonce( $response, $pagename . $type . $request['post_ID'] ) ) {
			return true;
		}
		return false;
	}

	public static function valid_post_id( int $post_id ): bool {
		$post = get_post( $post_id );
		if ( ! $post ) {
			return false;
		}
		$user = wp_get_current_user();
		if ( $user->ID && current_user_can( 'edit_post', $post->ID ) ) {
			return true;
		} elseif ( ! $user->ID && 0 === (int) $post->post_author && __( 'Auto Draft' ) === $post->post_title &&
		'auto-draft' === $post->post_status ) {
			return true;
		}
		return false;
	}
}

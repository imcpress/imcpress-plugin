<?php

namespace IMCPress\FrontendForm;
use IMCPress\FrontendForm\PublishPage;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class PostTypeFormGenerator {

	public array $form;
	public string $publish_page_slug;
	public string $post_type;
	public \WP_Post $post;
	public $captcha_prefix;

	public function __construct( string $type, string $publish_page_slug, array $supports, array $metaboxes, array $taxonomies, string $captcha_prefix, \WP_Post $post, string $type_label ) {
		$this->form['supports']   = $supports;
		$this->form['metaboxes']  = $metaboxes ?? array();
		$this->form['taxonomies'] = $taxonomies;
		$this->publish_page_slug  = strval( $publish_page_slug );

		$this->post_type = 'post' === $type ? $type : 'imcpress_' . $type;
		$this->type = $type;
		$this->post = $post;
		$this->captcha_prefix = $captcha_prefix;

		$this->form['html'] = $this->construct_form( $type, $type_label );
	}

	private function construct_form( string $type, string $type_label ): string {
		$form_action = '/' . $this->publish_page_slug . '/' . $type;
		$form_title = sprintf( __( 'Publish your %s', 'imcpress' ), mb_strtolower( $type_label ) );
		$html  = '<h1>' . $form_title . '</h1>';
		$html .= '<div class="form-type-description">';
		$html .= PublishPage::get_page_description( $this->type );
		$html .= '</div>';
		$html .= '<form name="post" action="' . $form_action . '" method="post" id="post">';
		$html .= '<div class="row">';
		$html .= '<div>';

		$html .= PostTypeFormElement::post_id( $this->post->ID );

		if ( in_array( 'title' , $this->form['supports'], true ) ) {
			$html .= PostTypeFormElement::title();
		}

		$author_metabox = $this->form['metaboxes'][0];
		unset( $this->form['metaboxes'][0] );

		foreach ( $this->form['metaboxes'] as $metabox ) {
			$html .= self::do_metabox( $metabox );
		}

		$html .= PostTypeFormElement::content();

		$html .= '</div>';
		$html .= '<div>';

		if ( ! (bool) get_user_meta( get_current_user_id(), 'imcpress_is_group', true) ) {
			$html .= self::do_metabox( $author_metabox );
		}

		foreach ( $this->form['taxonomies'] as $tax ) {
			$html .= PostTypeFormElement::taxonomy( $tax, $this->post );
		}
		if ( ! is_user_logged_in() ) {
			$html .= PostTypeFormElement::captcha( $this->captcha_prefix );
		}
		$html .= PostTypeFormElement::nonce( $this->publish_page_slug, $this->post_type, $this->post->ID) ;

		$html .= PostTypeFormElement::submit();

		$html .= '</div>';
		$html .= '</div>';

		$html .= '</form>';

		return $html;
	}

	private function do_metabox( array $metabox ): string {
		$html  = '<div id="' . $metabox['slug'] . '" class="postbox">';
		$html .= '<div class="inside">';
		$html .= '<p class="attribute-wrapper"><label class="post-attributes-label" for="' . $metabox['label'] . '">' . $metabox['label'] . '</label></p>';
		foreach ( $metabox['metadata'] as $metadata => $metadatum ) {
			$default = isset( $metadatum['default'] ) ? $metadatum['default'] : '';
			$html .= '<p>';
			$metabox_id = $this->post_type . '_metadata_' . $metadatum['slug'];
			$current_metabox = new \IMCPress\Metabox\Display( $metabox_id, $default, $metadatum );
			$html .= $current_metabox->html;
			$html .= '</p>';
			$meta_nonce = new \IMCPress\Metabox\Display(
				$metabox_id . '_nonce',
				wp_create_nonce( $metabox_id ),
				array(
					'type' => 'hidden',
				),
			);
			$html .= $meta_nonce->html;
		}
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	public function display_form(): string {
		return $this->form['html'];
	}
}

<?php
/*
 * Heavily forked on really-simple-captcha
 */

namespace IMCPress\FrontendForm;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Captcha {

	public string $chars;
	public int $char_length;
	public array $fonts;
	public string $tmp_dir;
	public array $img_size;
	public array $base;
	public int $font_size;
	public int $font_char_width;
	public int $file_mode;
	public int $answer_file_mode;

	public function __construct() {
		$this->chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789';
		$this->char_length = 6;
		$this->fonts = array(
			IMCPRESS_PATH . '/assets/fonts/GenBkBasR.ttf',
			IMCPRESS_PATH . '/assets/fonts/GenBkBasI.ttf',
			IMCPRESS_PATH . '/assets/fonts/GenBkBasBI.ttf',
			IMCPRESS_PATH . '/assets/fonts/GenBkBasB.ttf',
		);
		$this->tmp_dir = WP_CONTENT_DIR . '/tmp/';
		$this->img_size = array( 300, 100 );
		$this->base = array( 30, 60 );
		$this->font_size = 40;
		$this->font_char_width = 45;
		$this->file_mode = 0644;
		$this->answer_file_mode = 0640;
	}

	public function generate_random_word(): string {
		$word = '';

		for ( $i = 0; $i < $this->char_length; $i++ ) {
			$pos = wp_rand( 0, strlen( $this->chars ) - 1 );
			$char = $this->chars[ $pos ];
			$word .= $char;
		}

		return $word;
	}

	public function generate_image( string $prefix, string $word ): void {

		// That should be moved in a cronjob
		$this->cleanup();

		$filename = null;

		$image = imagecreatetruecolor(
			$this->img_size[0],
			$this->img_size[1]
		);

		if ( $image ) {
			$background_color = imagecolorallocate(
				$image,
				255,
				255,
				255,
			);

			imagefill( $image, 0, 0, $background_color );

			$x = $this->base[0] + mt_rand( -15, 15 );
			$word_length = strlen( $word );
			for ( $i = 0; $i < $word_length; $i++ ) {
				$foreground_color = imagecolorallocate(
					$image,
					wp_rand( 0, 128 ),
					wp_rand( 0, 128 ),
					wp_rand( 0, 128 ),
				);
				$font = $this->fonts[ array_rand( $this->fonts ) ];

				imagettftext(
					$image,
					$this->font_size,
					mt_rand( -20, 20 ),
					$x,
					$this->base[1] + mt_rand( -15, 15 ),
					$foreground_color,
					$font,
					$word[ $i ]
				);

				$x += $this->font_char_width;
			}

			for ( $i = 0; $i < 15; $i++ ) {
				$line_color = imagecolorallocate(
					$image,
					wp_rand( 0, 255 ),
					wp_rand( 0, 255 ),
					wp_rand( 0, 255 ),
				);
				imageline(
					$image,
					0,
					wp_rand( 0, 100 ),
					300,
					wp_rand( 0, 100 ),
					$line_color,
				);
			}

			for ( $i = 0; $i < 1500; $i++ ) {
				$pixel_color = imagecolorallocate(
					$image,
					wp_rand( 0, 255 ),
					wp_rand( 0, 255 ),
					wp_rand( 0, 255 ),
				);
				imagesetpixel(
					$image,
					wp_rand( 0, 300 ),
					wp_rand( 0, 100 ),
					$pixel_color
				);
			}

			$filename = sanitize_file_name( $prefix . '.png' );
			imagepng( $image, $this->tmp_dir . $filename );

			imagedestroy( $image );
			@chmod( $filename, $this->file_mode );
		}

		$this->generate_answer_file( $prefix, $word );
	}

	public function generate_answer_file( string $prefix, string $word ): void {
		$answer_file = $this->tmp_dir . sanitize_file_name( $prefix . '.txt' );

		if ( $filehandle = fopen( $answer_file, 'w' ) ) {
			$word = strtoupper( $word );
			$salt = wp_generate_password( 64 );
			$hash = hash_hmac( 'md5', $word, $salt );
			$code = $salt . '|' . $hash;
			fwrite( $filehandle, $code );
			fclose( $filehandle );
		}

		@chmod( $answer_file, $this->answer_file_mode );
	}

	public function check( string $prefix, string $response ): bool {
		if ( 0 === strlen( $prefix ) ) {
			return false;
		}

		$response = strtoupper( $response );

		$file = $this->tmp_dir . sanitize_file_name( $prefix . '.txt' );

		if ( is_readable( $file ) && $code = file_get_contents( $file ) ) {
			$code = explode( '|', $code, 2 );
			$salt = $code[0];
			$hash = $code[1];

			if ( hash_equals( $hash, hash_hmac( 'md5', $response, $salt ) ) ) {
				$this->remove( $prefix );
				return true;
			}
		}

		return false;
	}

	public function remove( string $prefix ): void {

		$files = array(
			sanitize_file_name( $this->tmp_dir . $prefix . '.png' ),
			sanitize_file_name( $this->tmp_dir . $prefix . '.txt' ),
		);

		foreach ( $files as $file ) {
			if ( is_file( $file ) ) {
				@unlink( $file );
			}
		}
	}

	/**
	 * Clean up dead files older than given length of time.
	 *
	 * @param int $minutes Consider older files than this time as dead files
	 * @return int|bool The number of removed files. Return false if error occurred.
	 */
	public function cleanup( int $minutes = 60, int $max = 100 ): void {
		if ( ! is_dir( $this->tmp_dir ) || ! is_readable( $this->tmp_dir ) ) {
			wp_die( 'Something is wrong with the captcha tmp directory' );
		}

		$count = 0;

		if ( $handle = opendir( $this->tmp_dir ) ) {
			while ( false !== ( $filename = readdir( $handle ) ) ) {
				if ( ! preg_match( '/^[0-9]+\.(php|txt|png)$/', $filename ) ) {
					continue;
				}

				$file = $this->tmp_dir . $filename;

				if ( ! file_exists( $file ) || ! $stat = stat( $file ) ) {
					continue;
				}

				if ( ( $stat['mtime'] + $minutes * 60 ) < time() ) {
					if ( ! unlink( $file ) ) {
						chmod( $file, 0644 );
						unlink( $file );
					}

					++$count;
				}

				if ( $max <= $count ) {
					break;
				}
			}

			closedir( $handle );
		}
	}
}

<?php

namespace IMCPress\Status;

use IMCPress\AbstractManager;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Manager extends AbstractManager {

	protected static string $managed_class = 'IMCPress\Status\Status';

}

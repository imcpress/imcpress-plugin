<?php

namespace IMCPress\Status;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Status {

	public string $slug;
	public array $args;

	public function __construct( $init_data ) {

		$this->slug = $init_data['slug'] ?? null;
		$this->args = $init_data['args'] ?? array();
		$this->set_pending_status_public();

	}

	public function register(): void {
		register_post_status( $this->slug, $this->args );

		add_action(
			'admin_footer',
			array(
				$this,
				'display_custom_post_status_option',
			)
		);

	}

	// Display Custom Post Status Option in Post Edit
	public function display_custom_post_status_option(): void {
		global $post;
		if ( current_user_can( 'publish_posts' ) ) {
			$complete = '';
			$label = $this->args['label'];
			if ( $post && $post->post_status === $this->slug ) {
					$complete = ' selected=\"selected\"';
			}
			echo( '
			<script id="test">
			jQuery(document).ready(function($){
					$("select#post_status").append("<option value=\"' . esc_attr( $this->slug ) . '\" ' . esc_attr( $complete ) . '>' . $label . '</option>");
					$("select[name=_status]").append("<option value=\"' . esc_attr( $this->slug ) . '\" ' . esc_attr( $complete ) . '>' . $label . '</option>");
			});
			</script>
			' );
		}
	}

	public static function set_pending_status_public() {
		register_post_status(
			'pending',
			array(
				'label'               => _x( 'Pending', 'post status' ),
				'protected'           => false,
				'public'              => true,
				'exclude_from_search' => false, // ?
				'_builtin'            => true, /* internal use only. */
				'label_count'         => _n_noop(
					'Pending <span class="count">(%s)</span>',
					'Pending <span class="count">(%s)</span>'
				),
				'date_floating'       => true,
			),
		);
	}
}

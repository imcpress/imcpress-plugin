<?php

namespace IMCPress\Taxonomy;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Taxonomy {

	public string $slug;
	public array $args;
	public string $label;
	public string $lc_label;

	private array $default_args = array(
		'hierarchical'          => false,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => true,
		'show_in_rest'          => false,
		'rest_base'             => null,
		'default_term'          => array(),
		'labels'                => array(),
	);

	public function __construct( array $init_data ) {

		$this->slug = (string) $init_data['slug'] ?? null;

		if ( ! isset( $init_data['labels']['name'] ) ) {
			$this->label = ucfirst( $this->slug );
			$this->lc_label = $this->label;
		} else {
			$this->label = $init_data['labels']['name'];
			$this->lc_label = mb_strtolower( $this->label );
		}

		$type = $init_data['type'] ?? null;
		unset( $init_data['type'] );

		$args = isset( $init_data['args'] ) ? (array) $init_data['args'] : array();
		$this->args = wp_parse_args( $args, $this->default_args );

		if ( $type ) {
			$this->args['meta_box_cb'] = array( '\IMCPress\Metabox\Display', 'taxonomy_' . $type );

		}

		$default_labels = array(
			'name'                  => $this->label,
			'singular_name'         => $this->label,
			'search_items'          => sprintf( __( 'Search a %s', 'imcpress' ), $this->lc_label ),
			'popular_items'         => sprintf( __( 'Popular %s', 'imcpress' ), $this->lc_label ),
			'all_items'             => __( 'All', 'imcpress' ),
			'parent_item'           => sprintf( __( 'Parent %s', 'imcpress' ), $this->label ),
			'parent_item_colon'     => sprintf( __( 'Parent %s', 'imcpress' ), $this->label ),
			'edit_item'             => __( 'Edit' ),
			'update_item'           => __( 'Update' ),
			'view_item'             => __( 'View' ),
			'add_new_item'          => sprintf( __( 'Add a New %s', 'imcpress' ), $this->lc_label ),
			'new_item_name'         => __( 'New' ),
			'add_or_remove_items'   => sprintf( __( 'Add or remove %s', 'imcpress' ), $this->lc_label ),
			'choose_from_most_used' => sprintf( __( 'Choose from the most used %s', 'imcpress' ), $this->lc_label ),
			'not_found'             => sprintf( __( '%s not found', 'imcpress' ), $this->label ),
			'no_terms'              => sprintf( __( 'No %s', 'imcpress' ), $this->lc_label ),
			'menu_name'             => $this->label,
			'items_list_navigation' => sprintf( __( '%s list navigation', 'imcpress' ), $this->label ),
			'items_list'            => sprintf( __( '%s list', 'imcpress' ), $this->label ),
			'most_used'             => sprintf( __( 'Most Used %s', 'imcpress' ), $this->lc_label ),
			'back_to_items'         => sprintf( '&larr; ' . __( 'Back to %s', 'imcpress' ), $this->lc_label ),
		);
		if ( $type !== 'dropdown' ) {
			$default_labels['separate_items_with_commas'] = sprintf( __( 'Separate %s with commas', 'imcpress' ), $this->lc_label );
		} else {
			$default_labels['separate_items_with_commas'] = sprintf( __( 'Pick a %s', 'imcpress' ), $this->lc_label );
		}

		$this->args['labels'] = $default_labels;
	}

	public function register(): void {
		register_taxonomy( $this->slug, array( 'post' ), $this->args );

		add_filter(
			'term_updated_messages',
			array(
				$this,
				'taxonomy_updated_messages',
			),
		);
	}

	public function taxonomy_updated_messages( array $messages ) {
		$messages[ $this->slug ] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __( '%s added.', 'imcpress' ), $this->label ),
			2 => sprintf( __( '%s deleted.', 'imcpress' ), $this->label ),
			3 => sprintf( __( '%s updated.', 'imcpress' ), $this->label ),
			4 => sprintf( __( '%s not added.', 'imcpress' ), $this->label ),
			5 => sprintf( __( '%s not updated.', 'imcpress' ), $this->label ),
			6 => sprintf( __( '%s deleted.', 'imcpress' ), $this->label ),
		);

		return $messages;
	}
}

<?php

namespace IMCPress\Taxonomy;

use IMCPress\AbstractManager;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Manager extends AbstractManager {

	protected static string $managed_class = 'IMCPress\Taxonomy\Taxonomy';

}

<?php

namespace IMCPress;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class MimeType {

	protected static array $allowed_mime_types = array(
		// Image formats.
		'jpg|jpeg|jpe' => 'image/jpeg',
		'gif'          => 'image/gif',
		'png'          => 'image/png',
		'webp'         => 'image/webp',
		// Video formats.
		'avi'          => 'video/avi',
		'mpeg|mpg|mpe' => 'video/mpeg',
		'mp4|m4v'      => 'video/mp4',
		'ogv'          => 'video/ogg',
		'webm'         => 'video/webm',
		'mkv'          => 'video/x-matroska',
		// Text formats.
		'txt|asc|srt'  => 'text/plain',
		'csv'          => 'text/csv',
		'tsv'          => 'text/tab-separated-values',
		'ics'          => 'text/calendar',
		// Audio formats.
		'mp3|m4a|m4b'  => 'audio/mpeg',
		'wav'          => 'audio/wav',
		'ogg|oga'      => 'audio/ogg',
		'flac'         => 'audio/flac',
		// Misc application formats.
		'rtf'          => 'application/rtf',
		'pdf'          => 'application/pdf',
		'tar'          => 'application/x-tar',
		'zip'          => 'application/zip',
		'gz|gzip'      => 'application/x-gzip',
		'rar'          => 'application/rar',
		'7z'           => 'application/x-7z-compressed',
		// MS Office formats.
		'doc'          => 'application/msword',
		'docx'         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		// OpenOffice formats.
		'odt'          => 'application/vnd.oasis.opendocument.text',
		'ods'          => 'application/vnd.oasis.opendocument.spreadsheet',
	);

	public static function restrict(): array {
		return self::$allowed_mime_types;
	}
}

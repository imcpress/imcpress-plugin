<?php

namespace IMCPress;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Sitemap {

	public static function plugins_loaded(): void {
		add_filter( 'wp_sitemaps_taxonomies', array( __CLASS__, 'remove_category' ) );
		add_filter( 'wp_sitemaps_users_query_args', array( __CLASS__, 'filter_groups' ) );
	}

	public static function remove_category( array $taxonomies ): array {
		unset( $taxonomies['category'] );
		return $taxonomies;
	}

	public static function filter_groups( array $args ): array {
		$args = array_merge(
			$args,
			array(
				'meta_key'   => 'imcpress_is_group',
				'meta_value' => '1'
			),
		);
		return $args;
	}
}

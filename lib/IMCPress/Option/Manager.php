<?php

namespace IMCPress\Option;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Manager {

	public function __construct() {}

	// Sets the menu and various hooks
	public static function plugins_loaded() {
		\IMCPress\Option\IMCPress::instance()->set_defaults_if_not_set();
		add_action( 'init', function() {
			\IMCPress\Option\IMCPress::instance()->set_disapproved_comments_option();
		}, 11);
		add_filter( 'pre_update_option',
			function( $value, $option ) {
				return \IMCPress\Option\Wordpress::instance()->lock_default_options( $value, $option );
			},
			11,
			2,
		);
		add_filter( 'sanitize_option_blogonion', array( __CLASS__, 'sanitize_blogonion' ) );
		new \IMCPress\Option\Menu();
	}

	// Sets default IMCPress && WP options
	public static function activate() {
		\IMCPress\Option\IMCPress::instance()->set_defaults_if_not_set();
		\IMCPress\Option\Wordpress::instance()->configure();
	}

	public static function sanitize_blogonion( $value ) {
		if ( substr( $value, 0, 7 ) !== 'http://' && ! empty( $value )) {
				return 'http://' . $value;
			}
		return $value;
	}
}

<?php

namespace IMCPress\Option;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Wordpress {

	private static $instance = null;
	protected array $plain_options;
	protected array $serialized_options;

	private function __construct() {
		$this->plain_options = array(
			'wordpress'      => array(
				'require_name_email'           => '',
				'use_smilies'                  => '',
				'default_pingback_flag'        => '',
				'default_ping_status'          => '',
				'default_comment_status'       => '',
				'comment_moderation'           => '',
				'comment_registration'         => '',
				'comment_previously_approved'  => '',
				'close_comments_for_old_posts' => '',
				'show_comments_cookies_opt_in' => '',
				'tag_base'                     => '/about',
				'default_role'                 => 'contributor',
				'permalink_structure'          => '/%post_id%/%postname%/',
				'show_avatars'                 => 1,
				'avatar_default'               => 'blank',

			),
			'classic-editor' => array(
				'classic-editor-allow-users' => 'disallow',
				'classic-editor-replace'     => 'classic',
			),
		);
		$this->serialized_options = array(
			'unbloater_settings' => array(
				'remove_update_available_notice'              => '1',
				'disallow_file_edit'                          => '0',
				'limit_post_revisions'                        => '1',
				'disable_application_passwords'               => '1',
				'limit_application_password_creation'         => '1',
				'disable_xmlrpc'                              => '1',
				'remove_generator_tag'                        => '1',
				'remove_wlw_manifest_link'                    => '1',
				'remove_rsd_link'                             => '1',
				'remove_feed_generator_tag'                   => '1',
				'remove_wporg_dns_prefetch'                   => '1',
				'disable_emojis'                              => '1',
				'disable_comment_hyperlinks'                  => '1',
				'reduce_heartbeat_interval'                   => '1',
				'block_editor_deactivate_block_directory'     => '1',
				'block_editor_deactivate_template_editor'     => '1',
				'block_editor_deactivate_core_block_patterns' => '1',
			),
		);
	}


	public static function instance(): \IMCPress\Option\Wordpress {
		if ( null === self::$instance ) {
			self::$instance = new Wordpress();
		}
		return self::$instance;
	}

	public function get_options(): array {
		return $this->options;
	}

	public function configure(): void {
		foreach ( $this->plain_options as $option_type ) {
			foreach ( $option_type as $option_name => $option_value ) {
				update_option( $option_name, $option_value );
			}
		}
		foreach ( $this->serialized_options as $option_name => $option_values ) {
			$current = (array) get_option( $option_name );
			if ( $current ) {
				$options = array_merge( $current, $option_values );
				update_option( $option_name, $options );
			} else {
				add_option( $option_name, $option_values );

			}
		}
	}

	public function lock_default_options( $value, string $option ) {
		if ( isset( $this->plain_options[ $option ] ) ) {
			return $this->plain_options[ $option ];
		} elseif ( isset( $this->serialized_options[ $option ] ) ) {
			return array_merge( $value, $this->serialized_options[ $option ] );
		}
		return $value;
	}
}

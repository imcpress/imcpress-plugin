<?php

namespace IMCPress\Option;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class IMCPress {

	private static $instance = null;
	protected array $imcpress_options;

	private function __construct() {
		$this->imcpress_options = array(
			'publish_page_slug'    => array(
				'default'     => 'publish',
				'type'        => 'text',
				'label'       => __( 'Publish page slug', 'imcpress' ),
				'desc'        => __(
					'Slug of the URL where the frontend form is displayed on your website',
					'imcpress'
				),
				'update_hook' => 'flush_rewrite_rules',
			),
			'contributor_login'    => array(
				'default' => '',
				'type'    => 'text',
				'label'   => __( 'Default contributor login', 'imcpress' ),
				'desc'    => __(
					'Login of the account that will be used to save the contributions posted from the frontend form',
					'imcpress'
				),
			),
			'blogonion'            => array(
				'default'         => '',
				'type'            => 'text',
				'label'           => __( 'Onion address', 'imcpress' ),
				'desc'            => __(
					'URL used to access the site over Tor (preprend with "http://")',
					'imcpress'
				),
				'element_options' => array(
					'required' => false,
				),
			),
			'disapproved_comments' => array(
				'default' => array(
					'wire'      => array(
						'global' => 'global',
					),
					'post_type' => array(
						'attachment' => 'attachment',
					),
					'status'    => array(
						'refused' => 'refused',
						'debate'  => 'debate',
						'pending' => 'pending',
					),
				),
				'type'    => 'multiple_checkboxes_categorized',
				'label'   => __( 'Disapproved comments', 'imcpress' ),
				'desc'    => __(
					'Define criterias for which comments on an article are by default disapproved',
					'imcpress'
				),
			),
			'disable_public_cron' => array(
				'default'         => false,
				'type'            => 'checkbox',
				'label'           => __( 'Disable wp_cron on page load', 'imcpress' ),
				'desc'            => __(
					'Do not spawn wp-cron on page load. Check this if you use another mechanism to run the wp-cron jobs, e.g cron or systemd.',
					'imcpress'
				),
				'element_options' => array(
					'required' => false,
				),
			),
		);
	}

	public function set_disapproved_comments_option(): void {

		foreach ( get_terms( 'wire', array( 'hide_empty' => false ) ) as $wire ) {
			$wires[ $wire->slug ] = $wire->slug;
		}
		foreach ( get_post_types_by_support( 'comments' ) as $type ) {
			$types[ $type ] = $type;
		}
		unset( $types['post'] );
		$stati = get_post_stati(
			array(
				'public' => true,
			)
		);

		$this->imcpress_options['disapproved_comments']['options'] = array(
			'wire'      => $wires,
			'post_type' => $types,
			'status'    => $stati,
		);
	}

	public static function instance(): \IMCPress\Option\IMCPress {
		if ( null === self::$instance ) {
			self::$instance = new IMCPress();
		}
		return self::$instance;
	}

	public function get_options(): array {
		return $this->imcpress_options;
	}

	public function set_defaults_if_not_set(): void {
		add_action( 'admin_init', array( $this, 'register_options' ) );
		foreach ( $this->imcpress_options as $option_name => $option_values ) {
			$current = get_option( $option_name );
			if ( ! $current ) {
				add_option( $option_name, $option_values['default'] );
			}
			if ( isset( $option_values['update_hook'] ) ) {
				add_action(
					'update_option_' . $option_name,
					array(
						$this,
						$option_values['update_hook'],
					),
				);
			}
		}
	}

	public function register_options(): void {
		foreach ( $this->imcpress_options as $option_name => $option_values ) {
			$sanitizer = new \IMCPress\Metabox\Sanitize( $option_values['type'] );
			register_setting(
				'default',
				$option_name,
				array(
					'type'              => 'string',
					'sanitize_callback' => array(
						$sanitizer,
						'sanitize',
					),
				),
			);
			add_settings_field(
				$option_name,
				$option_values['label'],
				function () use ( $option_name ) {
					$this->display_metabox( $option_name );
				},
				'IMCPress',
			);
		}
	}

	public function display_metabox( string $option_name ) {
		$option          = $this->imcpress_options[ $option_name ];
		$element_options = $option['element_options'] ?? array();
		unset( $option['label'], $option['default'], $option['element_options'] );
		$value   = get_option( $option_name );
		$metabox = new \IMCPress\Metabox\Display(
			$option_name,
			$value ?? '',
			$option,
			$element_options
		);
		echo $metabox->html;
	}

	public static function flush_rewrite_rules(): void {
		delete_option( 'rewrite_rules' );
	}
}

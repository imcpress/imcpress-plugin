<?php

namespace IMCPress\Option;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Menu {

	public function __construct() {
		add_action( 'admin_init', array( $this, 'add_settings' ) );
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
	}

	public function add_settings() {
		add_settings_section(
			'default',
			__( 'Default options', 'imcpress' ),
			array(
				$this,
				'display_section',
			),
			'IMCPress',
		);
	}

	public function admin_menu(): void {
		add_options_page(
			__( 'IMCPress options', 'imcpress' ),
			'IMCPress',
			'manage_options',
			'IMCPress',
			array(
				$this,
				'settings_page',
			)
		);
	}

	public function display_section() {
		echo '';
	}

	public function settings_page() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die();
		}
		?>
		<div class="wrap">
			<h2><?php _e( 'IMCPress options', 'imcpress' ); ?></h2>
			<form action="options.php" method="POST">
				<?php settings_fields( 'default' ); ?>
				<?php do_settings_sections( 'IMCPress' ); ?>
				<?php submit_button(); ?>
			</form>
		</div>
		<?php
	}
}

<?php

namespace IMCPress;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class UI {

	public static function plugins_loaded(): void {

		add_action( 'admin_print_styles', array( __CLASS__, 'hide_sticky_post' ) );
		add_filter( 'dashboard_recent_posts_query_args', array( __CLASS__, 'dashboard_recent_posts_query_args' ) );

		// these should probably be moved into IMCPress\Type so that they
		//  can be factorized to have all the custom metadata displayed the
		//  same way, i.e the author_display_name one
		add_action( 'manage_imcpress_event_posts_columns', array( __CLASS__, 'manage_event_columns' ) );
		add_action( 'manage_edit-imcpress_event_sortable_columns', array( __CLASS__, 'manage_sortable_event_columns' ) );
		add_action( 'manage_imcpress_event_posts_custom_column', array( __CLASS__, 'manage_event_columns_value' ), 10, 2 );
		add_action( 'pre_get_posts', array( __CLASS__, 'set_sortable_order' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'clean_media_uploader_menu' ) );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'clean_media_uploader_menu' ) );
	}

	public static function dashboard_recent_posts_query_args( array $query_args ): array {
		$query_args['post_type'] = 'any';
		if ( 'future' === $query_args['post_status'] ) {
			$query_args['post_status']    = 'pending';
			$query_args['posts_per_page'] = 12;
		}
		return $query_args;
	}

	// Hide sticky posts box (as we don't use them)
	// From https://wordpress.stackexchange.com/questions/10676/is-there-a-way-to-disable-the-sticky-posts-feature
	public static function hide_sticky_post() {
		global $pagenow;
		if ( ! in_array( $pagenow, array( 'post.php', 'post-new.php', 'edit.php' ), true ) ) {
			return;
		}
		?>
		<style type="text/css">
			#sticky-span { display:none!important }
			input[name="sticky"], input[name="sticky"] + span{ display:none!important; }
		</style>
		<?php
	}

	// Fix: should move to Type + factorize
	public static function manage_event_columns( $columns ) {
		$columns['imcpress_event_metadata_date']  = __( 'Event date:', 'imcpress' );
		$columns['imcpress_event_metadata_place'] = __( 'Event place:', 'imcpress' );
		return $columns;
	}

	public static function manage_sortable_event_columns( $columns ) {
		$columns['imcpress_event_metadata_date']  = 'imcpress_event_metadata_date';
		$columns['imcpress_event_metadata_place'] = 'imcpress_event_metadata_place';
		return $columns;
	}

	public static function manage_event_columns_value( $column, $post_id ) {
		switch ( $column ) {
			case 'imcpress_event_metadata_date':
				$wp_datetime_format = get_option( 'date_format' ) . ' H:i';
				$date               = new \DateTime( get_post_meta( $post_id, $column, true ) );
				$value              = $date->format( $wp_datetime_format );
				break;
			case 'imcpress_event_metadata_place':
				$value = \sanitize_text_field( get_post_meta( $post_id, $column, true ) );
				break;
		}
		echo $value;
	}

	public static function set_sortable_order( \WP_Query $query ): void {
		global $pagenow;

		if ( is_admin() && ! defined('DOING_AJAX') && $pagenow !== 'customize.php' && did_action( 'wp_loaded' ) < 1 ) {
			if ( function_exists( 'get_current_screen' ) ) {
				// Make event meta sorting effective
				$screen = get_current_screen();
				if ( 'edit-imcpress_event' === $screen->id ) {
					$order = $query->get( 'orderby' );

					if ( 'imcpress_event_metadata_date' === $order ) {
						$query->set( 'meta_key', 'imcpress_event_metadata_date' );
						$query->set( 'orderby', 'meta_value' );
					}
					if ( 'imcpress_event_metadata_place' === $order ) {
						$query->set( 'meta_key', 'imcpress_event_metadata_place' );
						$query->set( 'orderby', 'meta_value' );
					}
				}
			}
		}
	}

	public static function clean_media_uploader_menu(): void {
		wp_enqueue_style(
			'clean_media_uploader',
			IMCPRESS_URL . 'assets/css/clean_media_uploader_menu.css',
		);
	}
}

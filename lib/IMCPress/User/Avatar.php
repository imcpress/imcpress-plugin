<?php

namespace IMCPress\User;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

/**
 * Custom Avatar Without a Plugin, from:
 * https://userswp.io/wordpress-profile-picture/
 */

class Avatar {

	public static function plugins_loaded(): void {
		add_action( 'admin_enqueue_scripts',  array(__CLASS__, 'enqueue_avatar_scripts'));
		add_action( 'admin_print_footer_scripts-profile.php', array( __CLASS__, 'admin_media_scripts') );
		add_action( 'admin_print_footer_scripts-user-edit.php', array( __CLASS__, 'admin_media_scripts') );
		add_action( 'show_user_profile', array( __CLASS__, 'custom_user_profile_fields') );
		add_action( 'edit_user_profile', array( __CLASS__, 'custom_user_profile_fields') );
		add_action( 'personal_options_update', array( __CLASS__, 'save_local_avatar_fields') );
		add_action( 'edit_user_profile_update', array( __CLASS__, 'save_local_avatar_fields') );

		add_filter( 'get_avatar_url', array( __CLASS__,'get_avatar_url'), 10, 3 );
	}

	public static function enqueue_avatar_scripts( $hook ){
		// Load scripts only on the profile page.
		if( $hook === 'profile.php' || $hook === 'user-edit.php' )
		{
			add_thickbox();
			wp_enqueue_script( 'media-upload' );
			wp_enqueue_media();
		}
	}

	public static function admin_media_scripts() {
		// Also look at https://wordpress.stackexchange.com/questions/32785/how-to-remove-these-fields-from-the-profile-section/40844#40844
		?>
		<script>
			jQuery(document).ready(function ($) {
				$(document).on('click', '.avatar-image-upload', function (e) {
					e.preventDefault();
					var $button = $(this);
					var file_frame = wp.media.frames.file_frame = wp.media({
						title: 'Select or Upload an Custom Avatar',
						library: {
							type: 'image' // mime type
						},
						button: {
							text: 'Select Avatar'
						},
						multiple: false
					});
					file_frame.on('select', function() {
						var attachment = file_frame.state().get('selection').first().toJSON();
						$button.siblings('#imcpress_avatar').val( attachment.sizes.thumbnail.url );
						$button.siblings('.custom-avatar-preview').attr( 'src', attachment.sizes.thumbnail.url );
					});
					file_frame.open();
				});
			});

            jQuery("tr.user-profile-picture").remove();
		</script>
		<?php
	}

	public static function custom_user_profile_fields( $profileuser ) {
		if ( get_user_meta( $profileuser->ID, 'imcpress_is_group', true) == 1 )
		{
			?>
			<h3>Custom avatar</h3>
			<table class="form-table imcpress-avatar-upload-options">
				<tr>
					<th>
						<label for="image">Custom local avatar</label>
					</th>
					<td>
						<?php
						// Check whether we saved the custom avatar, else return the default avatar.
						$custom_avatar = get_the_author_meta( 'imcpress_avatar', $profileuser->ID );
						if ( $custom_avatar == '' )
						{
							$custom_avatar = get_avatar_url( $profileuser->ID );
						}
						else
						{
							$custom_avatar = esc_url_raw( $custom_avatar );
						}
						?>
						<img style="width: 96px; height: 96px; display: block; margin-bottom: 15px;" class="custom-avatar-preview" src="<?php echo $custom_avatar; ?>">
						<input type="text" name="imcpress_avatar" id="imcpress_avatar" value="<?php echo esc_attr( esc_url_raw( get_the_author_meta( 'imcpress_avatar', $profileuser->ID ) ) ); ?>" class="regular-text" />
						<input type='button' class="avatar-image-upload button-primary" value="Upload image" id="uploadimage"/><br />
						<span class="description">
							Please upload a custom avatar for your profile, to remove the avatar simple delete the URL and click update.
						</span>
					</td>
				</tr>
			</table>
			<?php
		}
	}

	public static function save_local_avatar_fields( $user_id ) {
		if ( current_user_can( 'edit_user', $user_id ) && get_user_meta( $user_id, 'imcpress_is_group', true) == 1 )
		{
			if( isset($_POST[ 'imcpress_avatar' ]) )
			{
				$avatar = esc_url_raw( $_POST[ 'imcpress_avatar' ] );
				update_user_meta( $user_id, 'imcpress_avatar', $avatar );
			}
		}
	}

	public static function get_avatar_url( $url, $id_or_email, $args ) {
		$id = '';
		if ( is_numeric( $id_or_email ) )
		{
			$id = (int) $id_or_email;
		}
		elseif ( is_object( $id_or_email ) )
		{
			if ( !empty( $id_or_email->user_id ) )
			{
				$id = (int) $id_or_email->user_id;
			}
		}
		else
		{
			$user = get_user_by( 'email', $id_or_email );
			$id = !empty( $user ) ?  $user->data->ID : '';
		}
		//Preparing for the launch.
		$custom_url = $id ?  get_user_meta( $id, 'imcpress_avatar', true ) : '';
		
		// If there is no custom avatar set, return the normal one.
		if ( $custom_url == '' || !empty($args['force_default']))
		{
			// Fix: set default avatar by constant/config?
			return esc_url_raw( '/wp-content/themes/imcpress-theme/assets/img/icons/user.png' ); 
		}
		else
		{
			return esc_url_raw($custom_url);
		}
	}

}

<?php

namespace IMCPress\User;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Registration {
	private const DUMMY = 'dummy@localhost.localdomain';

	public static function plugins_loaded(): void {
		add_action( 'register_form', array( __CLASS__, 'register_form' ) );

		add_filter( 'user_registration_email', array( __CLASS__, 'email_trick' ) );
		add_filter( 'email_exists', array( __CLASS__, 'email_exists' ), 10, 2 );

		add_filter( 'registration_errors', array( __CLASS__, 'form_validation' ), 10, 3 );
		add_action( 'register_new_user', array( __CLASS__, 'registered_user' ), 10, 2 );
	}

	public static function register_form(): void {
		_e( 'You can set an empty email', 'imcpress' );

		$prefix  = \IMCPress\FrontendForm\PublishPage::generate_captcha();
		$captcha = \IMCPress\FrontendForm\PostTypeFormElement::captcha( $prefix );

		?>
		<p>
			<label for="user_password"><?php _e( 'Password' ); ?></label>
			<input type="password" name="user_password" id="user_password" class="input" minlength="8" />
		</p>
		<p>
			<label for="user_password_confirm"><?php _e( 'Confirm your password' ); ?></label>
			<input type="password" name="user_password_confirm" id="user_password_confirm" class="input" minlength="8" />
		</p>
		<p>
			<?php echo $captcha; ?>
		</p>
		<?php
	}

	public static function form_validation( \WP_Error $errors, string $sanitized_user_login, string $user_email ): \WP_Error {
		if ( ! \IMCPress\FrontendForm\PostTypeFormValidator::check_captcha( (array) $_REQUEST ) ) {
			$errors->add( 'wrong-captcha', __( 'Wrong captcha', 'imcpress' ) );
		}

		if ( isset( $_REQUEST['user_password'] ) && ! empty( $_REQUEST['user_password'] ) ) {
			$pass = trim( $_REQUEST['user_password'] );

			if ( mb_strlen( $pass ) < 8 ) {
				$errors->add( 'bad-password', __( 'Your password must contain at least 8 characters', 'imcpress' ) );
			} elseif ( ! preg_match( '#[0-9]+#', $pass ) ) {
				$errors->add( 'bad-password', __( 'Your password must contain at least 1 number', 'imcpress' ) );
			} elseif ( ! preg_match( '#[A-Z]+#', $pass ) ) {
				$errors->add( 'bad-password', __( 'Your password must contain at least 1 uppercase letter', 'imcpress' ) );
			} elseif ( ! preg_match( '#[a-z]+#', $pass ) ) {
				$errors->add( 'bad-password', __( 'Your password must contain at least 1 lowercase letter', 'imcpress' ) );
			} elseif ( ! preg_match( '#[\W]+#', $pass ) ) {
				$errors->add( 'bad-password', __( 'Your password must contain at least 1 special character', 'imcpress' ) );
			}
		} else {
			$errors->add( 'no-password', __( 'No password entered', 'imcpress' ) );
		}

		if ( isset( $_REQUEST['user_password_confirm'] ) && ! empty( $_REQUEST['user_password_confirm'] ) ) {
			if ( trim( $_REQUEST['user_password'] ) !== trim( $_REQUEST['user_password_confirm'] ) ) {
				$errors->add( 'password_mismatch', __( 'Your passwords don\'t match', 'imcpress' ) );
			}
		} else {
			$errors->add( 'no-password-confirm', __( 'No password confirmation entered', 'imcpress' ) );
		}

		return $errors;
	}

	public static function registered_user( int $user_id ): void {
		$user = get_userdata( $user_id );
		if ( $user->user_email === self::DUMMY ) {
			wp_update_user(
				array(
					'ID'         => $user_id,
					'user_email' => '',
				)
			);
		}

		$pass = trim( $_REQUEST['user_password'] );
		wp_set_password( $pass, $user_id );
		update_user_meta( $user_id, 'default_password_nag', false );
	}

	public static function email_trick( string $email ): string {
		if ( empty( $email ) ) {
			return self::DUMMY;
		}
		return $email;
	}

	public static function email_exists( $user_id, string $email ) {
		if ( $email === self::DUMMY ) {
			return false;
		}
		return $user_id;
	}

}

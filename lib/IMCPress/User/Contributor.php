<?php

namespace IMCPress\User;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Contributor {
	public string $role;
	public array $post_types;
	public bool $wpadmin_lockout;
	private \WP_User $user;

	public function __construct( $post_types, $wpadmin_lockout ) {
		$this->role            = 'contributor';
		$this->post_types      = $post_types;
		$this->wpadmin_lockout = $wpadmin_lockout;
		$this->user            = wp_get_current_user();
	}

	public function init(): void {

		if ( $this->wpadmin_lockout ) {
			add_action( 'init', array( $this, 'public_blockwpadmin' ) );
			add_action( 'init', array( $this, 'public_hide_admin_bar' ) );
		} else {
			add_action( 'user_register', array( $this, 'hide_meta_boxes' ) );
			if ( is_admin() && in_array( $this->role, $this->user->roles, true ) ) {
				add_action( 'pre_get_posts', array( $this, 'admin_show_only_user_posts' ) );
				add_action( 'comments_clauses', array( $this, 'admin_show_only_user_comments' ) );
				add_filter( 'screen_options_show_screen', array( $this, 'admin_hide_options_screen' ) );
				add_action( 'admin_init', array( $this, 'admin_tools_redirect' ) );
				add_action( 'admin_init', array( $this, 'admin_hide_tools' ) );
				add_action( 'admin_init', array( $this, 'admin_remove_dashboard_widgets' ) );
				add_action( 'user_has_cap', array( $this, 'disable_custom_status_editing' ), 0, 4 );
				add_action(
					'comment_row_actions',
					function( $actions, $comment ) {
						return array();
					},
					100,
					2
				);
			}
		}
	}

	public function block_wp_admin(): void {
		if ( is_admin() && in_array( $this->role, $this->user->roles, true ) && ! defined( 'DOING_AJAX' ) ) {
			wp_safe_redirect( home_url() );
			exit;
		}
	}

	public function public_hide_admin_bar(): void {
		if ( in_array( $this->role, $this->user->roles, true ) ) {
			show_admin_bar( false );
		}
	}

	public static function add_caps( array $caps, string $role ): void {
		$contributors = get_role( $role );
		foreach ( $caps as $cap ) {
			if ( ! $contributors->has_cap( $cap ) ) {
				$contributors->add_cap( $cap );
			}
		}
	}

	public static function remove_caps( array $caps, string $role ): void {
		$contributors = get_role( $role );
		foreach ( $caps as $cap ) {
			if ( $contributors->has_cap( $cap ) ) {
				$contributors->remove_cap( $cap );
			}
		}
	}

	public function admin_show_only_user_posts( \WP_Query $query ): void {
		$user = wp_get_current_user();
		$query->set( 'author', $user->ID );
	}

	public function admin_show_only_user_comments( array $clauses ): array {
		global $user_ID, $wpdb;
		$clauses['where'] .= ' AND ' . $wpdb->base_prefix .'comments.user_id = ' . $user_ID;
		return $clauses;
	}

	public function admin_tools_redirect(): void {
		$current_url = add_query_arg( null, null );
		$parts       = explode( '/', $current_url );
		$last        = end( $parts );
		$admin_pages = array(
			'tools.php',
		);
		if ( in_array( $last, $admin_pages, true ) ) {
			wp_safe_redirect( admin_url() );
			exit;
		}
	}

	public function admin_hide_tools(): void {
		remove_menu_page( 'tools.php' );
	}

	public function admin_hide_options_screen(): bool {
		return false;
	}

	public function admin_remove_dashboard_widgets(): void {
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	}

	public function hide_meta_boxes( int $user_id ): void {
		$user = get_user_by( 'ID', $user_id );
		if ( in_array( 'contributor', $user->roles, true ) ) {
			$hidden_meta_boxes = array(
				'postimagediv',
				'postcustom',
				'commentstatusdiv',
				'slugdiv',
				'trackbacksdiv',
				'postexcerpt',
			);
			$post_types = $this->post_types;
			$post_types[] = 'post';
			foreach ( $post_types as $post_type ) {
				$imcpress_post_type = 'post' === $post_type ? $post_type : 'imcpress_' . $post_type;
				add_user_meta(
					$user_id,
					'metaboxhidden_' . $imcpress_post_type,
					$hidden_meta_boxes,
					true,
				);
			}
		}
	}

	public function disable_custom_status_editing( array $allcaps, array $caps, array $args, \WP_User $user ): array {
		$editable_statuses = array(
			'pending',
			'draft',
			'auto-draft',
		);
		if ( in_array( 'contributor', $user->roles, true ) ) {
			$cap     = $args[0];
			$post_id = isset( $args[2] ) ? $args[2] : 0;

			if ( 0 === $post_id ) {
				global $post, $typenow, $pagenow, $current_screen;
				$post_id = isset( $_REQUEST['post'] ) ? (int) $_REQUEST['post'] : false;
				$post_type = null;

				if ( $post && $post->post_type ) {
					$post_type = $post->post_type;
				} elseif ( $typenow ) {
					$post_type = $typenow;
				} elseif ( $current_screen && ! empty( $current_screen->post_type ) ) {
					$post_type = sanitize_key( $current_screen->post_type );
				} elseif ( isset( $_REQUEST['post_type'] ) && ! empty( $_REQUEST['post_type'] ) ) {
					$post_type = sanitize_key( $_REQUEST['post_type'] );
				} elseif ( 'post.php' === $pagenow && ! empty( $post_id ) ) {
					$post_type = get_post_type( (int) $post_id );
				} elseif ( 'edit.php' === $pagenow && empty( $_REQUEST['post_type'] ) ) {
					$post_type = 'post';
				}
			} else {
				$post_type = get_post_type( $post_id );
			}

			$obj = get_post_type_object( $post_type );

			if ( ! $obj || 'revision' === $obj->name ) {
				return $allcaps;
			}

			$caps_to_modify = array(
				$obj->cap->edit_post,
				'edit_post',
				$obj->cap->edit_others_posts,
			);
			if ( ! in_array( $cap, $caps_to_modify, true ) ) {
				return $allcaps;
			}

			if ( ! is_user_logged_in() ) {
				return $allcaps;
			}

			$post_status = get_post_status( $post_id );
			if ( ! in_array( $post_status, $editable_statuses, true ) ) {
				$allcaps[ $obj->cap->edit_posts ] = false;
				return $allcaps;
			}
		}
		return $allcaps;
	}
}

<?php

namespace IMCPress\User;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Group {

	public static function plugins_loaded(): void {
		add_action( 'show_user_profile', array( __CLASS__, 'custom_user_profile_field') );
		add_action( 'edit_user_profile', array( __CLASS__, 'custom_user_profile_field') );
		add_action( 'user_new_form', array( __CLASS__, 'custom_user_profile_field') );
		add_action( 'personal_options_update', array( __CLASS__, 'save_is_group_field') );
		add_action( 'edit_user_profile_update', array( __CLASS__, 'save_is_group_field') );
		add_action( 'user_register', array( __CLASS__, 'save_is_group_field') );
	}


	public static function custom_user_profile_field( $user ) {
		if ( current_user_can('edit_users') )
		{
			$is_group = isset($user->ID) ? get_user_meta( $user->ID, 'imcpress_is_group', true) : 0;

			?>
			<h3>Group</h3>
			<table class="form-table">
				<tr>
					<th>
						<label for="imcpress_is_group">Is user group</label>
					</th>
					<td>					
						<input type="checkbox" name="imcpress_is_group" id="imcpress_is_group" value="1"<?=  $is_group ? ' checked':'' ?>/><label for="imcpress_is_group"> User is a group</label>
					</td>
				</tr>
			</table>
		<?php
		}
	}

	public static function save_is_group_field( $user_id ) {
		if ( current_user_can( 'edit_users' ) )
		{
			if( isset($_POST[ 'imcpress_is_group' ]) )
			{
				$is_group = $_POST[ 'imcpress_is_group' ] == 1 ? 1 : 0;
				update_user_meta( $user_id, 'imcpress_is_group', $is_group );
			}
			else
			{
				update_user_meta( $user_id, 'imcpress_is_group', 0 );
			}
		}
	}

}

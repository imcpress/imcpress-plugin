<?php

namespace IMCPress\User;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Hyperactive {

	public function __construct() {}

	public static function authenticate( object $user, string $username, string $password ): object {
		if ( is_wp_error( $user ) ) {
			$tested_user = get_user_by( 'login', sanitize_user( $username ) );
			if ( $tested_user and self::has_hyperactive_password( $tested_user ) ) {
				if ( self::authenticate_hyperactive_password( $tested_user->user_pass, $password ) ) {
					wp_set_password( $password, $tested_user->ID );
					return $tested_user;
				}
			}
		}
		return $user;
	}

	private static function has_hyperactive_password( \WP_User $user ): bool {
		$password_pattern = '/^\$1\$/';
		return preg_match( $password_pattern, $user->user_pass ) ? true : false;
	}

	private static function authenticate_hyperactive_password( string $hyperactive_pass, string $password ): string {
		$exploded_pass    = explode( '$', $hyperactive_pass );
		$hyperactive_salt = $exploded_pass[2];
		$hyperactive_hash = $exploded_pass[3];
		return md5( $password . $hyperactive_salt, false ) == $hyperactive_hash ? true : false;
	}
}

<?php

namespace IMCPress;

defined( 'ABSPATH' ) || die( 'Invalid request.' );

class Query {

	public static function plugins_loaded(): void {
		add_action( 'pre_get_posts', array( __CLASS__, 'adapt_the_query' ) );
	}

	public static function adapt_the_query( \WP_Query $query ): void {
		global $wp, $wp_query;

		// Block search
		if ( get_query_var( 's' ) ) {
			$wp_query->is_search = false;
			$wp_query->is_home = true;
		}

		if ( $query->is_main_query() && ! is_admin() && empty( get_query_var( 'p' ) ) ) {
			// Archive: list published and featured posts
			if ( $query->is_archive() ) {
				$query->set( 'post_status', array( 'feature', 'publish' ) );
			}

			// Archive: list all types for groups, and taxonomy if hook is set
			if ( ( $query->is_tax() || $query->is_tag ) && ! $query->is_post_type_archive() ) {
				if ( apply_filters( 'only_post_on_tax_archive', true ) ) {
					$query->set( 'post_type', array( 'post' ) );
				} else {
					$query->set( 'post_type', array( 'any' ) );
				}
			} elseif ( $query->is_author() ) {
				// In fact, this would also list pages and so, but those should not be associated with any tax
				$query->set( 'post_type', array( 'any' ) );
			}

			// /ctp/tax/term (eg: /events/in/somewhere)
			if ( $query->is_tax() && $query->is_post_type_archive ) {
				$query_var = get_query_var( 'taxonomy' );
				switch ( $query_var ) {
					case 'in':
						$tax = 'place_tag';
						break;
					case 'about':
						$tax = 'post_tag';
						break;
					default:
						$tax = '';
				}

				if ( ! empty( $tax ) ) {
					$query->set( 'taxonomy', $tax );
					$term = get_query_var( 'term' );

					add_filter(
						'get_the_archive_description',
						function() use ( $term, $tax ): string {
							return 'Filtered by <a class="tag" href="' .
								get_term_link( $term, $tax ) . '">' .
								get_query_var( 'term' ) .
								'</a>'; // Link to /tax/term
						}
					);
				}
			}

			// Events: order by event date
			if ( ! empty( get_query_var( 'post_type' ) ) && 'imcpress_event' === get_query_var( 'post_type' ) ) {
				$query->set( 'post_status', array( 'publish', 'feature', 'pending' ) );
				$query->set( 'meta_key', 'imcpress_event_metadata_date' );
				$query->set( 'orderby', 'meta_value' );
				$query->set( 'order', 'ASC' );

				if ( $query->is_tax() || ! empty( get_query_var( 'event_view' ) ) ) {
					// /events/archive/ + /events/tax/term
					if ( $query->is_tax() || 'archive' === get_query_var( 'event_view' ) ) {
						$query->set( 'order', 'DESC' );
					} else // calendar or list query
					{
						$year  = get_query_var( 'year' );
						$month = get_query_var( 'monthnum' );
						$query->set( 'year', '' );
						$query->set( 'monthnum', '' );
						$query->set( 'posts_per_page', '-1' );

						// If no date specified in URI
						$date = ( ! $year || ! $month ) ?
							new \DateTime() :
							new \DateTime( sprintf( '%04d-%02d-01', $year, $month ) );

						$query->set( 'meta_type', 'DATETIME' );
						$query->set( 'meta_compare', 'BETWEEN' );
						$query->set(
							'meta_value',
							array(
								$date->format( 'Y-m-01' ),
								$date->modify( '+1 month' )->format( 'Y-m-01' ),
							)
						);
					}
				} else {
					// To get only those from and after today:
					$query->set( 'meta_type', 'DATETIME' );
					$query->set( 'meta_value', ( new \Datetime() )->format( 'c' ) );
					$query->set( 'meta_compare', '>=' );
				}
			}

			// Status pages
			if ( $status = $wp->query_vars['post_status'] ?? false ) {
				$wp_query->is_archive    = true;
				$wp_query->is_home       = false;
				$wp_query->is_front_page = false;

				$types   = get_post_types( array( '_builtin' => false ) );
				$types[] = 'post';

				// As /publish is yet taken for publish page
				if ( 'published' === $status ) {
					$query->set( 'post_status', 'publish' );
					// is it possible to add pending events?
					// maybe not important as nothing links to this page
				} else if ( 'pending' === $status ) {
					// all pending but events
					unset( $types[ 'imcpress_event' ] );
				}

				add_filter(
					'get_the_archive_title',
					function( $status ): string {
						$status = get_query_var( 'post_status' );
						// Fix: i18n: this won't get parsed by wp-cli i18n command
						return __( ucfirst( $status ), 'imcpress' );
					}
				);

				$query->set( 'post_type', $types );
			}

			if ( is_feed() )
			{
				if ( isset( $query->query['list'] ) OR ( array_key_exists( 'feed', $query->query ) AND 1 === count( $query->query ) ) )
				{
					$wp_query->set( 'post_status', array( 'feature', 'publish' ) );
					$wp_query->set( 'post_type', 'any' );
				}
			}
		}
	}
}

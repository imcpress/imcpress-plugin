<?php
/**
 * IMCPress
 *
 * Plugin Name: IMCPress
 * Plugin URI: https://0xacab.org/imcpress/imcpress-plugin
 * Description: Turns WordPress into an IMC.
 * Version: 0.0.1
 * Author: IMCPress
 * Author URI: https://0xacab.org/imcpress
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Text Domain: imcpress
 * Domain Path: /languages
 * Requires at least: 5.8.1
 * Tested up to: 5.8.2
 * Requires PHP: 7.4
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation. You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 */

namespace IMCPress;

defined( 'ABSPATH' ) or die('Invalid request.');

define( 'IMCPRESS_PATH', plugin_dir_path( __FILE__ ) );
define( 'IMCPRESS_URL', esc_url( plugins_url( '', __FILE__ ) . '/' ) );

/* Classes autoloading */
\spl_autoload_register(function ($classname) {
	$classname = ltrim($classname, '\\');
	$filename = str_replace('\\', DIRECTORY_SEPARATOR, $classname);
	
	$path = IMCPRESS_PATH . 'lib/' . $filename . '.php';
	
	if (file_exists($path)) {
		require $path;
	}

}, true);

register_activation_hook( __FILE__, array( '\IMCPress\IMCPress', 'activate' ) );
register_deactivation_hook( __FILE__, array( '\IMCPress\IMCPress', 'deactivate' ) );

add_action( 'plugins_loaded', array( '\IMCPress\IMCPress', 'plugins_loaded' ) );

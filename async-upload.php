<?php

if ( isset( $_REQUEST['action'] ) && 'upload-attachment' === $_REQUEST['action'] ) {
	define( 'DOING_AJAX', true );
}

if ( ! defined( 'WP_ADMIN' ) ) {
	define( 'WP_ADMIN', true );
}

require_once '../../../wp-config.php';

header( 'Content-Type: text/plain; charset=' . get_option( 'blog_charset' ) );

if ( isset( $_REQUEST['action'] ) && 'upload-attachment' === $_REQUEST['action'] ) {

	require_once ABSPATH . 'wp-admin/includes/media.php';
	require_once ABSPATH . 'wp-admin/includes/file.php';
	require_once ABSPATH . 'wp-admin/includes/image.php';
	require_once ABSPATH . 'wp-admin/includes/ajax-actions.php';

	send_nosniff_header();
	nocache_headers();

	upload_attachment();
	die( '0' );

} else {
	echo wp_json_encode(
		array(
			'success' => false,
			'data'    => array(
				'message'  => 'Wrong action',
			),
		)
	);
	wp_die();
}

function upload_attachment() {
	check_ajax_referer( 'media-form' );

	if ( isset( $_REQUEST['post_id'] ) && ! empty( $_REQUEST['post_id'] ) ) {
		$post_id = (int) $_REQUEST['post_id'];
	} else {
		echo wp_json_encode(
			array(
				'success' => false,
				'data'    => array(
					'message'  => 'Invalid post ID',
				),
			)
		);
		wp_die();
	}

	$post_data = ! empty( $_REQUEST['post_data'] ) ? _wp_get_allowed_postdata( _wp_translate_postdata( false, (array) $_REQUEST['post_data'] ) ) : array();

	if ( is_wp_error( $post_data ) ) {
		wp_die( $post_data->get_error_message() );
	}

	$attachment_id = media_handle_upload( 'async-upload', $post_id, $post_data );

	if ( is_wp_error( $attachment_id ) ) {
		echo wp_json_encode(
			array(
				'success' => false,
				'data'    => array(
					'message'  => $attachment_id->get_error_message(),
					'filename' => esc_html( $_FILES['async-upload']['name'] ),
				),
			)
		);
		wp_die();
	}

	$attachment = wp_prepare_attachment_for_js( $attachment_id );
	if ( ! $attachment ) {
		wp_die();
	}

	echo wp_json_encode(
		array(
			'success' => true,
			'data'    => $attachment,
		)
	);
	wp_die();
}

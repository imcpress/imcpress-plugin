<?php

// Custom post status
$imcpress_statuses = array(
	array(
		'slug' => 'feature',
		'args' => array(
			'label'                     => __( 'Feature', 'imcpress' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop(
				'Feature <span class="count">(%s)</span>',
				'Feature <span class="count">(%s)</span>'
			),
		),
	),
	array(
		'slug' => 'refused',
		'args' => array(
			'label'  => __( 'Refused', 'imcpress' ),
			'public' => true,
			'exclude_from_search'       => true, //?
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop(
				'Refused <span class="count">(%s)</span>',
				'Refused <span class="count">(%s)</span>'
			),
		),
	),
	array(
		'slug' => 'debate',
		'args' => array(
			'label'  => __( 'Debating', 'imcpress' ),
			'public' => true,
			'exclude_from_search'       => true, //?
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop(
				'Debating <span class="count">(%s)</span>',
				'Debating <span class="count">(%s)</span>'
			),
		),
	),
);

// Taxonomies
$imcpress_taxonomies = array(
	array(
		'slug'   => 'place_tag',
		'labels' => array(
			'name' => __( 'Place tags', 'imcpress' ),
		),
		'args'   => array(
			'hierarchical' => false,
			'rewrite'	=> array(
				'slug'	=> 'in'
			)
		),
	),
	array(
		'slug'   => 'wire',
		'labels' => array(
			'name' => __( 'Wire', 'imcpress' ),
		),
		'type'   => 'dropdown',
		'args'   => array(
			'hierarchical' => false,
			'default_term' => array(
				array(
					'name'        => __( 'Local', 'imcpress' ),
					'slug'        => 'local',
					'description' => 'Info locales',
				),
				array(
					'name'        => __( 'Global', 'imcpress' ),
					'slug'        => 'global',
					'description' => 'Info globales',
				),
			),
			'rewrite'	=> array(
				'slug'	=> 'posts'
			)
		),
	),
);

// Types
$imcpress_post_types['event'] = array(
	'label'         => __( 'Event', 'imcpress' ),
	'supports'      => array(
		'title',
		'thumbnail',
		'editor',
		'author',
		'custom-fields',
		'comments',
	),
	'menu_position' => 6,
	'menu_icon'     => 'dashicons-calendar',
	'taxonomy_slug' => array( 'post_tag', 'place_tag' ),
	'metaboxes'     => array(
		array(
			'slug'     => 'details',
			'label'    => __( 'Event details', 'imcpress' ),
			'position' => 'side',
			'metadata' => array(
				array(
					'slug'  => 'date',
					'label' => __( 'Event date:', 'imcpress' ),
					'type'  => 'datetime',
				),
				array(
					'slug'  => 'place',
					'label' => __( 'Event place:', 'imcpress' ),
					'type'  => 'textarea',
				),
			),
		),
	),
);

$imcpress_post_types['zine'] = array(
	'label'         => __( 'Zine', 'imcpress' ),
	'supports'      => array(
		'title',
		'editor',
		'author',
		'custom-fields',
		'thumbnail',
		'comments',
	),
	'menu_position' => 8,
	'menu_icon'     => 'dashicons-media-interactive',
	'taxonomy_slug' => array( 'post_tag' ),
	'require_attachment'	=> true,
);

$imcpress_post_types['tumble'] = array(
	'label'         => __( 'Tumble', 'imcpress' ),
	'supports'      => array(
		'editor',
		'author',
		'custom-fields',
		'thumbnail',
	),
	'menu_position' => 7,
	'menu_icon'     => 'dashicons-testimonial',
	'taxonomy_slug' => array( 'post_tag', 'place_tag' ),
);
$imcpress_post_types['post'] = array(
	'label'         => __( 'Post', 'imcpress' ),
	'slug'          => 'posts',
	'supports'      => array(
		'title',
		'thumbnail',
		'editor',
		'author',
		'custom-fields',
		'comments',
	),
	'taxonomy_slug' => array( 'wire', 'post_tag', 'place_tag' ),
);

if ( file_exists( IMCPRESS_CONFIG_DIR . 'imcpress.php' ) ) {
	include IMCPRESS_CONFIG_DIR . 'imcpress.php';
}
